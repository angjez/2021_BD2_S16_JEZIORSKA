# 2021_BD2_S16_JEZIORSKA

## Ski station system

Task description:

Sprzedaż biletów, karnetów; rejestrowanie wykorzystania wyciągów dla narciarzy, definiowanie rozkładu wyciągów; możliwość blokowania biletu; raporty zarządcze oraz operacyjne w tym wydruki dla narciarzy.

## How-to

1. Open only via VS code workspace file.
1. Create a .env file in the project root and paste `.env.template` file content into it.
   [image](https://user-images.githubusercontent.com/43811039/115952873-e8cc4080-a4e8-11eb-9739-a4b374f80391.png)
1. Type `yarn start` into terminal in the project root.
