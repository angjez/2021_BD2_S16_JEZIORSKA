import GlobalStyle from "@src/config/styles/global";
import Loading from "@src/modules/Loading";
import routes from "@src/modules/Routing/routes";
import React, { FunctionComponent, Suspense, lazy } from "react";
import { Route, Switch } from "react-router-dom";
import Navbar from "../Routing/Navbar";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const routeComponents = routes.map((route) => {
  const RouteComponent = lazy(route.component);
  return (
    <Route path={route.path}>
      <RouteComponent />
    </Route>
  );
});

const App: FunctionComponent = () => {
  return (
    <>
      <GlobalStyle />
      <ToastContainer />
      <Suspense fallback={<Loading />}>
        <Navbar>
          <Switch>{routeComponents}</Switch>
        </Navbar>
      </Suspense>
    </>
  );
};

export default App;
