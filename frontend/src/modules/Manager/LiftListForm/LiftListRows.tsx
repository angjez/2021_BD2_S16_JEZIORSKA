import { FunctionComponent, useCallback, useState, useEffect } from "react";
import { HStack, Tr, Td } from "@chakra-ui/react";
import {
  AddLiftButton,
  EditLiftButton,
  DeleteLiftButton,
  StatusButton,
} from "@src/modules/Manager/LiftListForm/LiftListItems";
import { Lift } from "@src/global/types/Lift.types";
import axios from "axios";
import { toast } from "react-toastify";

interface LiftListItemProps {
  lift: Lift;
  refetch: () => Promise<void>;
}

export const LiftListItem: FunctionComponent<LiftListItemProps> = ({
  lift,
  refetch,
}: LiftListItemProps) => {
  const [entriesToday, setEntriesToday] = useState<number>();
  const [entriesLastWeek, setEntriesLastWeek] = useState<number>();
  const [entriesLastMonth, setEntriesLastMonth] = useState<number>();

  const editLift = useCallback(async (lift: Lift) => {
    try {
      const url = `http://localhost:4000/lift/${lift.id}`;
      const response = await axios.put<Lift>(url, lift);
      switch (response.status) {
        case 400:
          toast.error("Cannot change lift parameters. Try again later");
          break;
        case 200:
          break;
      }
    } catch (error) {
    } finally {
      await refetch();
      await getEntries();
    }
  }, []);

  const deleteLift = useCallback(async (id: number) => {
    try {
      const url = `http://localhost:4000/lift/${id}`;
      const response = await axios.delete(url);
      switch (response.status) {
        case 404:
          toast.error("Cannot delete lift");
          break;
        case 200:
          break;
      }
    } catch (error) {
    } finally {
      await refetch();
      await getEntries();
    }
  }, []);

  const getEntries = useCallback(async () => {
    try {
      const urlEntriesToday = `http://localhost:4000/entry/numberOfDays/${
        lift.id
      }${0}`;
      const todayResponse = await axios.get<number>(urlEntriesToday);
      switch (todayResponse.status) {
        case 200:
          setEntriesToday(todayResponse.data);
      }
      const urlEntriesLastWeek = `http://localhost:4000/entry/numberOfDays/${
        lift.id
      }${7}`;
      const weekResponse = await axios.get<number>(urlEntriesLastWeek);
      switch (weekResponse.status) {
        case 200:
          setEntriesLastWeek(weekResponse.data);
      }
      const urlEntriesLastMonth = `http://localhost:4000/entry/numberOfDays/${
        lift.id
      }${28}`;
      const monthResponse = await axios.get<number>(urlEntriesLastMonth);
      switch (monthResponse.status) {
        case 200:
          setEntriesLastMonth(monthResponse.data);
      }
    } catch (error) {}
  }, []);
  useEffect(() => {
    getEntries();
  }, []);

  return (
    <Tr>
      <Td>
        <HStack>
          <DeleteLiftButton id={lift.id} deleteLiftHandler={deleteLift} />
          <EditLiftButton lift={lift} editLiftHandler={editLift} />
        </HStack>
      </Td>
      <Td>{lift.name}</Td>
      <Td>
        <StatusButton lift={lift} changeLiftStatusHandler={editLift} />
      </Td>
      <Td>{entriesToday}</Td>
      <Td>{entriesLastWeek}</Td>
      <Td>{entriesLastMonth}</Td>
    </Tr>
  );
};

interface AddButtonTableLineProps {
  addLiftTableLineHandler: () => void;
}

export const AddButtonTableLine: FunctionComponent<AddButtonTableLineProps> = ({
  addLiftTableLineHandler,
}: AddButtonTableLineProps) => {
  return (
    <Tr>
      <Td>
        <AddLiftButton addLiftHandler={addLiftTableLineHandler} />
      </Td>
      <Td></Td>
      <Td></Td>
      <Td></Td>
      <Td></Td>
      <Td></Td>
    </Tr>
  );
};
