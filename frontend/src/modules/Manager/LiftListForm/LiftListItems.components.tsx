import colors from "@src/config/styles/colors";
import styled from "styled-components";
import { Button } from "@chakra-ui/react";
import { CloseIcon, CheckCircleIcon } from "@chakra-ui/icons";

export const StyledAddLiftButton = styled(Button)`
  width: 100px !important;
  color: ${colors.white} !important;
  background-color: ${colors.green} !important;
`;

export const StyledEditLiftButton = styled(Button)`
  width: 100px !important;
  color: ${colors.black} !important;
  border-color: ${colors.black} !important;
`;

export const StyledDeleteLiftButton = styled(Button)`
  width: 100px !important;
  color: ${colors.white} !important;
  background-color: ${colors.red} !important;
`;

export const StyledStatusButton = styled(Button)`
  width: 100px !important;
  border-color: ${colors.black} !important;
`;

export const StyledCheckCircleIcon = styled(CheckCircleIcon)`
  font-size: 24px;
  color: ${colors.green} !important;
`;

export const StyledCloseIcon = styled(CloseIcon)`
  font-size: 24px;
  color: ${colors.red} !important;
`;
