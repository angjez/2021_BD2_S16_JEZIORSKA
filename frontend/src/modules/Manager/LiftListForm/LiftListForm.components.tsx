import colors from "@src/config/styles/colors";
import styled from "styled-components";
import { Table, Th } from "@chakra-ui/react";

export const StyledTable = styled(Table)`
  font-size: 18px;
  color: ${colors.black} !important;
  background-color: ${colors.white} !important;
`;

export const TableHeader = styled(Th)`
  font-size: 14px !important;
  color: ${colors.black} !important;
`;
