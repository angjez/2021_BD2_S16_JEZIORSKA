import { FunctionComponent, useState, useCallback, useEffect } from "react";
import {
  useDisclosure,
  HStack,
  Thead,
  Tbody,
  Tfoot,
  Tr,
} from "@chakra-ui/react";
import { StyledTable, TableHeader } from "./LiftListForm.components";
import { AddLiftModal } from "@src/modules/Manager/LiftListForm/Modals";
import {
  LiftListItem,
  AddButtonTableLine,
} from "@src/modules/Manager/LiftListForm/LiftListRows";
import { Lift } from "@src/global/types/Lift.types";
import axios from "axios";

export const LiftList: FunctionComponent = () => {
  const {
    isOpen: isOpenAddModal,
    onOpen: onOpenAddModal,
    onClose: onCloseAddModal,
  } = useDisclosure();

  const [liftList, setLiftList] = useState<Lift[]>([]);

  const getLifts = useCallback(async () => {
    try {
      const url = "http://localhost:4000/lift";
      const response = await axios.get<Lift[]>(url);
      switch (response.status) {
        case 200:
          response.data.sort((a, b) => a.name.localeCompare(b.name));
          setLiftList(response.data);
      }
    } catch (error) {}
  }, []);
  useEffect(() => {
    getLifts();
  }, []);

  return (
    <HStack>
      <StyledTable variant="striped">
        <Thead>
          <Tr>
            <TableHeader></TableHeader>
            <TableHeader>Lift Name</TableHeader>
            <TableHeader>Lift Status</TableHeader>
            <TableHeader>Entries Today</TableHeader>
            <TableHeader>Entries In Last 7 Days</TableHeader>
            <TableHeader>Entries In Last Month</TableHeader>
          </Tr>
        </Thead>
        <Tbody>
          {liftList?.map((item, index) => (
            <LiftListItem lift={item} refetch={getLifts} key={index} />
          ))}
          <AddButtonTableLine addLiftTableLineHandler={onOpenAddModal} />
        </Tbody>
        <Tfoot></Tfoot>
      </StyledTable>
      {/* Reports */}
      <AddLiftModal
        isOpen={isOpenAddModal}
        onClose={onCloseAddModal}
        refetch={getLifts}
      />
    </HStack>
  );
};
