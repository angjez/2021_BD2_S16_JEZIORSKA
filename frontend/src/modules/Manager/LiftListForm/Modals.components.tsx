import colors from "@src/config/styles/colors";
import styled from "styled-components";
import { Button } from "@chakra-ui/react";

export const ModalOkButton = styled(Button)`
  color: ${colors.white} !important;
  background-color: ${colors.blue} !important;
  margin-right: 10px;
`;

export const ModalCancelButton = styled(Button)`
  color: ${colors.black} !important;
  background-color: ${colors.lighterGrey} !important;
`;
