import React, { FunctionComponent, useState, useCallback, useRef } from "react";
import {
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Input,
} from "@chakra-ui/react";
import { Lift } from "@src/global/types/Lift.types";
import { ModalOkButton, ModalCancelButton } from "./Modals.components";
import axios from "axios";
import { toast } from "react-toastify";

interface AddLiftModalProps {
  isOpen: boolean;
  onClose: () => void;
  refetch: () => Promise<void>;
}

export const AddLiftModal: FunctionComponent<AddLiftModalProps> = ({
  isOpen,
  onClose,
  refetch,
}: AddLiftModalProps) => {
  const [newNameValid, setNewNameValid] = useState<boolean>(true);
  const [newLiftName, setNewLiftName] = useState<string>("");

  const addLift = useCallback(async (name: string) => {
    try {
      const url = "http://localhost:4000/lift";
      const response = await axios.post<Lift[]>(url, {
        name: name,
        isOperational: false,
      });
      switch (response.status) {
        case 400:
          toast.error("Cannot add new lift. Try again later");
          break;
        case 201:
          break;
      }
    } catch (error) {
    } finally {
      onClose();
      await refetch();
    }
  }, []);

  const validateName = () => {
    if (newLiftName.length > 0) {
      addLift(newLiftName);
    } else {
      setNewNameValid(false);
    }
  };

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Add new lift</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl>
              <FormLabel>New lift name</FormLabel>
              <Input
                placeholder="Lift name"
                onChange={(event) => setNewLiftName(event.target.value)}
                borderColor={newNameValid ? "black" : "red"}
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <ModalOkButton onClick={validateName}>Add</ModalOkButton>
            <ModalCancelButton onClick={onClose}>Cancel</ModalCancelButton>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

interface EditLiftModalProps {
  isOpen: boolean;
  lift: Lift;
  onClose: () => void;
  EditLiftHandler: (lift: Lift) => Promise<void>;
}

export const EditLiftModal: FunctionComponent<EditLiftModalProps> = ({
  isOpen,
  lift,
  onClose,
  EditLiftHandler,
}: EditLiftModalProps) => {
  const [newNameValid, setNewNameValid] = useState<boolean>(true);
  const [currentLiftName, setCurrentLiftName] = useState<string>(lift.name);

  const onCloseWithRefresh = () => {
    setNewNameValid(true);
    setCurrentLiftName(lift.name);
    onClose();
  };

  const confirmChanges = () => {
    if (currentLiftName.length > 0) {
      lift.name = currentLiftName;
      EditLiftHandler(lift);
      onCloseWithRefresh();
    } else {
      setCurrentLiftName(lift.name);
      setNewNameValid(false);
    }
  };

  return (
    <>
      <Modal isOpen={isOpen} onClose={onCloseWithRefresh}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Change lift name</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl>
              <FormLabel>New lift name</FormLabel>
              <Input
                value={currentLiftName}
                onChange={(event) => setCurrentLiftName(event.target.value)}
                borderColor={newNameValid ? "black" : "red"}
              />
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <ModalOkButton onClick={confirmChanges}>Change</ModalOkButton>
            <ModalCancelButton onClick={onCloseWithRefresh}>
              Cancel
            </ModalCancelButton>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};
