import React, { FunctionComponent } from "react";
import { useDisclosure, Stack } from "@chakra-ui/react";
import { DeleteIcon, AddIcon, EditIcon } from "@chakra-ui/icons";
import {
  StyledAddLiftButton,
  StyledEditLiftButton,
  StyledDeleteLiftButton,
  StyledStatusButton,
  StyledCheckCircleIcon,
  StyledCloseIcon,
} from "./LiftListItems.components";
import { EditLiftModal } from "@src/modules/Manager/LiftListForm/Modals";
import { Lift } from "@src/global/types/Lift.types";

interface AddLiftButtonProps {
  addLiftHandler: () => void;
}

export const AddLiftButton: FunctionComponent<AddLiftButtonProps> = ({
  addLiftHandler,
}: AddLiftButtonProps) => {
  return (
    <StyledAddLiftButton
      leftIcon={<AddIcon />}
      onClick={addLiftHandler}
      variant="solid"
    >
      Add
    </StyledAddLiftButton>
  );
};

interface EditLiftButtonProps {
  lift: Lift;
  editLiftHandler: (lift: Lift) => Promise<void>;
}

export const EditLiftButton: FunctionComponent<EditLiftButtonProps> = ({
  lift,
  editLiftHandler,
}: EditLiftButtonProps) => {
  const {
    isOpen: isOpenEditModal,
    onOpen: onOpenEditModal,
    onClose: onCloseEditModal,
  } = useDisclosure();

  return (
    <Stack>
      <StyledEditLiftButton
        leftIcon={<EditIcon />}
        onClick={onOpenEditModal}
        variant="outline"
      >
        Edit
      </StyledEditLiftButton>
      <EditLiftModal
        isOpen={isOpenEditModal}
        onClose={onCloseEditModal}
        lift={lift}
        EditLiftHandler={editLiftHandler}
      />
    </Stack>
  );
};

interface DeleteLiftButtonProps {
  id: number;
  deleteLiftHandler: (id: number) => Promise<void>;
}

export const DeleteLiftButton: FunctionComponent<DeleteLiftButtonProps> = ({
  id,
  deleteLiftHandler,
}: DeleteLiftButtonProps) => {
  return (
    <StyledDeleteLiftButton
      leftIcon={<DeleteIcon />}
      onClick={() => deleteLiftHandler(id)}
      variant="solid"
    >
      Delete
    </StyledDeleteLiftButton>
  );
};

interface StatusButtonProps {
  lift: Lift;
  changeLiftStatusHandler: (lift: Lift) => Promise<void>;
}

export const StatusButton: FunctionComponent<StatusButtonProps> = ({
  lift,
  changeLiftStatusHandler,
}: StatusButtonProps) => {
  const changeLiftStatus = () => {
    if (lift.isOperational) {
      lift.isOperational = false;
    } else {
      lift.isOperational = true;
    }
    changeLiftStatusHandler(lift);
  };

  return (
    <StyledStatusButton
      leftIcon={
        lift.isOperational ? <StyledCheckCircleIcon /> : <StyledCloseIcon />
      }
      onClick={changeLiftStatus}
      variant="outline"
    ></StyledStatusButton>
  );
};
