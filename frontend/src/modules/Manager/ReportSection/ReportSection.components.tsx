import colors from "@src/config/styles/colors";
import styled from "styled-components";
import { Button } from "@chakra-ui/react";

export const ReportButton = styled(Button)`
  color: ${colors.white} !important;
  background-color: ${colors.blue} !important;
  margin-right: 10px;
  width: 200px !important;
  font-size: 24px;
`;
