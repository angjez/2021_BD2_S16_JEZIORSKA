import { FunctionComponent, useCallback } from "react";
import axios from "axios";
import { ReportButton } from "./ReportSection.components";

export const ReportSection: FunctionComponent = () => {
  const getReport = useCallback(async () => {
    try {
      const url = "http://localhost:4000/report/liftReport";
      axios({
        url: url,
        method: "GET",
        responseType: "blob",
      }).then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "Lift_entries_report.pdf");
        document.body.appendChild(link);
        link.click();
      });
    } catch (error) {}
  }, []);

  return (
    <ReportButton onClick={getReport}>Generate entries report</ReportButton>
  );
};
