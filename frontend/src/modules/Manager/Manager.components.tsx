import { VStack, HStack } from "@chakra-ui/react";
import styled from "styled-components";

export const MainStack = styled(VStack)`
  margin-top: 20px;
  margin-right: 20px;
  margin-left: 20px;
  margin-bottom: 70px;
`;

export const SubStack = styled(HStack)`
  spacing: 10;
`;
