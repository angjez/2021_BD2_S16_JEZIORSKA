import { FunctionComponent } from "react";
import { ChakraProvider, Heading, Button } from "@chakra-ui/react";
import { MainStack, SubStack } from "@src/modules/Manager/Manager.components";
import { LiftList } from "./LiftListForm/LiftListForm";
import { ReportSection } from "./ReportSection/ReportSection";

const Manager: FunctionComponent = () => (
  <ChakraProvider>
    <MainStack>
      <Heading>Lift Management</Heading>
      <SubStack>
        <LiftList />
        <ReportSection />
      </SubStack>
    </MainStack>
  </ChakraProvider>
);

export default Manager;
