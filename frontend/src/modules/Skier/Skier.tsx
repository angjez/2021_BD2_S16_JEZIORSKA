import { ChakraProvider, Heading } from "@chakra-ui/react";
import { MainStack } from "@src/modules/Skier/Skier.components";
import TicketForm from "@src/modules/Skier/TicketForm/TicketForm";
import React, { FunctionComponent } from "react";

const Skier: FunctionComponent = () => (
  <ChakraProvider>
    <MainStack>
      <Heading>Ticket Validation</Heading>
      <TicketForm />
    </MainStack>
  </ChakraProvider>
);

export default Skier;
