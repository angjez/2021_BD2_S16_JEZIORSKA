import styled from "styled-components";
import { Button, FormControl, Select, } from "@chakra-ui/react";

export const StyledFormControl = styled(FormControl)`
    margin-top: 20px;
`;

export const StyledSelect = styled(Select)`
    width: 500px;
`;

export const StyledButton = styled(Button)`
    margin-top: 20px;
    width: 500px !important;
    background-color: #1A72FF !important;
    color: #fff;
`;