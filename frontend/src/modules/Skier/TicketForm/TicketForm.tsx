import { toast, useDisclosure } from "@chakra-ui/react";
import {
  SkipassInput,
  TicketButton,
  TicketLiftPicker,
} from "@src/modules/Skier/TicketForm/Inputs";
import {
  FailureEntryModal,
  SuccessEntryModal,
} from "@src/modules/Skier/TicketForm/Modals";
import { StyledFlex } from "@src/modules/Skier/TicketForm/TicketForm.components";
import React, { FunctionComponent, useState, useCallback, useEffect } from "react";
import { Lift } from "@src/global/types/Lift.types";
import { Entry } from "@src/global/types/Entry.types";
import axios from "axios";

var temp = false;

const TicketForm: FunctionComponent = () => {
  const {
    isOpen: isOpenSuccessModal,
    onOpen: onOpenSuccessModal,
    onClose: onCloseSuccessModal,
  } = useDisclosure();

  const {
    isOpen: isOpenFailureModal,
    onOpen: onOpenFailureModal,
    onClose: onCloseFailureModal,
  } = useDisclosure();
  const [liftName, setLiftName] = useState("");
  const [skipassId, setSkipassId] = useState(-1);
  const [selectedliftId, setLiftId] = useState(-1);

  const [liftList, setLiftList] = useState<Lift[]>([]);

  const getLifts = useCallback(async() => {
    try {
      const url = "http://localhost:4000/lift/operational";
      const response = await axios.get<Lift[]>(url);
      switch(response.status) {
        case 200:
          setLiftList(response.data);
      }
    } catch(error) {}
  }, []);

  useEffect(() => {
    getLifts();
  }, []);

  const addEntry = useCallback(async(boughtSkipassId: number, liftId: number) => {
    try {
      const url = "http://localhost:4000/entry";
      const response = await axios.post<Entry[]>(url, {
        boughtSkipassId: boughtSkipassId,
        liftId: liftId,
        createdAt: new Date(),
      });

      switch(response.status) {
        case 201:
          onOpenSuccessModal();
          break;
      }
    } catch(error) {
      onOpenFailureModal();
    }
  }, []);

  var submit = () => {
    console.log(skipassId);
    console.log(selectedliftId);
    if(skipassId != -1 && !isNaN(skipassId)
      && liftName != "") {
      console.log("adding");
      addEntry(skipassId, selectedliftId);
    }
      
  };

  return (
    <StyledFlex>
      <form onSubmit={(e) => e.preventDefault()}>
        <TicketLiftPicker
          liftList={liftList}
          liftNameHandler={setLiftName}
          liftIdHandler={setLiftId}
        />
        <SkipassInput skipassHandler={setSkipassId} />
        <TicketButton validationHandler={submit} />
      </form>
      <SuccessEntryModal
        skipassId={skipassId}
        liftName={liftName}
        isOpen={isOpenSuccessModal}
        onClose={onCloseSuccessModal}
      />
      <FailureEntryModal
        skipassId={skipassId}
        liftName={liftName}
        isOpen={isOpenFailureModal}
        onClose={onCloseFailureModal}
      />
    </StyledFlex>
  );
};

export default TicketForm;
