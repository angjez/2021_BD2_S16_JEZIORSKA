import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/react";
import {
  FailureButton,
  FailureModalContent,
  SuccessButton,
  SuccessModalContent,
} from "@src/modules/Skier/TicketForm/Modals.components";
import { FunctionComponent } from "react";

interface EntryModalProps {
  skipassId: number;
  liftName: string;
  isOpen: boolean;
  onClose: () => void;
}

export const SuccessEntryModal: FunctionComponent<EntryModalProps> = ({
  skipassId,
  liftName,
  isOpen,
  onClose,
}: EntryModalProps) => (
  <Modal isOpen={isOpen} onClose={onClose}>
    <ModalOverlay />
    <SuccessModalContent>
      <ModalHeader>Successful Entry</ModalHeader>
      <ModalCloseButton />
      <ModalBody>
        {" "}
        Your skipass(id. {skipassId}) is valid. Now you can go and enjoy skiing
        on {liftName}.{" "}
      </ModalBody>
      <ModalFooter>
        <SuccessButton onClick={onClose}>OK</SuccessButton>
      </ModalFooter>
    </SuccessModalContent>
  </Modal>
);

export const FailureEntryModal: FunctionComponent<EntryModalProps> = ({
  skipassId,
  liftName,
  isOpen,
  onClose,
}: EntryModalProps) => (
  <Modal isOpen={isOpen} onClose={onClose}>
    <ModalOverlay />
    <FailureModalContent>
      <ModalHeader>Entry failed.</ModalHeader>
      <ModalCloseButton />
      <ModalBody>
        Your skipass(id. {skipassId}) is not valid. Please go to cash register
        and buy a new one to use {liftName}.
      </ModalBody>

      <ModalFooter>
        <FailureButton onClick={onClose}>OK</FailureButton>
      </ModalFooter>
    </FailureModalContent>
  </Modal>
);
