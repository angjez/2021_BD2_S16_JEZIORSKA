import { FormLabel, NumberInput, NumberInputField } from "@chakra-ui/react";
import {
  StyledButton,
  StyledFormControl,
  StyledSelect,
} from "@src/modules/Skier/TicketForm/Inputs.components";
import { FunctionComponent } from "react";
import { Lift } from "@src/global/types/Lift.types";

interface SkipassInputProps {
  skipassHandler: (id: number) => void;
}

export const SkipassInput: FunctionComponent<SkipassInputProps> = ({
  skipassHandler,
}: SkipassInputProps) => (
  <StyledFormControl id="skipassId" isRequired>
    <FormLabel>Skipass ID</FormLabel>
    <NumberInput onChange={(s, n): void => skipassHandler(n)}>
      <NumberInputField placeholder="SkipassID" />
    </NumberInput>
  </StyledFormControl>
);

interface TicketLiftPickerProps {
  liftList: Lift[];
  liftNameHandler: (name: string) => void;
  liftIdHandler: (id: number) => void;
}

export const TicketLiftPicker: FunctionComponent<TicketLiftPickerProps> = ({
  liftList,
  liftNameHandler,
  liftIdHandler,
}: TicketLiftPickerProps) => (
  <StyledSelect
    placeholder="Select lift"
    onChange={(e: React.ChangeEvent<any>): void => {
        liftNameHandler(e.target.value);
        if(e.currentTarget.selectedIndex - 1 >= 0) {
          liftIdHandler(liftList[e.currentTarget.selectedIndex - 1].id);
        }
        
      }
    }
  >
    {liftList.map(function (d, idx) {
      return (
        <option key={idx} id={String(d.id)} value={d.name}>
          {d.name}
        </option>
      );
    })}
  </StyledSelect>
);

interface TicketButtonProps {
  validationHandler: () => void;
}

export const TicketButton: FunctionComponent<TicketButtonProps> = ({
  validationHandler,
}: TicketButtonProps) => {
  return (
    <StyledButton onClick={validationHandler} type="submit">
      Validate
    </StyledButton>
  );
};
