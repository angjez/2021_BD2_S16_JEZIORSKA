import styled from "styled-components";
import { Button, ModalContent, } from "@chakra-ui/react";

export const SuccessModalContent = styled(ModalContent)`
    color: #ffffff !important;
    background-color: green !important;
`;

export const FailureModalContent = styled(ModalContent)`
    color: #ffffff !important;
    background-color: #ff0000 !important;
`;

export const SuccessButton = styled(Button)`
    color: #fff;
    margin-right: 6px;
    background-color: green !important;
`;

export const FailureButton = styled(Button)`
    color: #fff;
    margin-right: 6px;
    background-color: #ff0000 !important;
`;