import { Flex, } from "@chakra-ui/react";
import styled from "styled-components";


export const StyledFlex = styled(Flex)`
    width: 600px;
    align-items: center;
    justify-content: center;
    border-width: 1px;
    border-radius: 8px;
    box-shadow: 8px;
    padding: 60px 20px;
`;