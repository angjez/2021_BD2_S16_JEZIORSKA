import { Skipass } from "@src/global/types/Skipass.types";
import SkipassItem from "@src/modules/Owner/ViewSkipasses/SkipassItem";
import { ViewSkipassesWrapper } from "@src/modules/Owner/ViewSkipasses/ViewSkipasses.components";
import axios from "axios";
import React, { FunctionComponent, useCallback } from "react";
import { toast } from "react-toastify";

interface Props {
  skipasses: Skipass[];
  refetch: () => Promise<void>;
  updateSkipass: (skipass: Skipass) => void;
}

const ViewSkipasses: FunctionComponent<Props> = ({
  skipasses,
  refetch,
  updateSkipass,
}) => {
  const deleteSkipass = useCallback(async (id: number) => {
    try {
      const url = `http://localhost:4000/skipass/${id}`;
      const response = await axios.delete(url);
      switch (response.status) {
        // 400 - bad request (form error)
        case 404:
          toast.error("No such skipass.");
          break;
        case 201:
          break;
      }
    } catch (error) {
    } finally {
      await refetch();
    }
  }, []);

  return (
    <ViewSkipassesWrapper>
      {skipasses.map((el) => (
        <SkipassItem
          skipass={el}
          key={el.id}
          deleteSkipass={deleteSkipass}
          updateSkipass={updateSkipass}
        />
      ))}
    </ViewSkipassesWrapper>
  );
};

export default ViewSkipasses;
