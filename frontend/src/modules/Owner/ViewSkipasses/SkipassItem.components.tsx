import colors from "@src/config/styles/colors";
import { defaultElementBorderRadius } from "@src/config/styles/mixins";
import styled from "styled-components";

export const SkipassWrapper = styled.div`
  border: 1px solid ${colors.blue};
  border-radius: 4px;
  :hover {
    border: 1px solid ${colors.navy};
  }
  padding: 10px 10px 10px;
  margin: 10px 10px 10px;
`;

export const SkipassNameText = styled.div`
  font-size: 18px;
  color: ${colors.blue};
  text-align: justify;

  & {
    margin-bottom: 10px;
  }
`;

export const SkipassText = styled.div`
  font-size: 14px;
  color: ${colors.black};
  text-align: justify;
`;

export const SkipassBoldText = styled.div`
  font-weight: 600;
  font-size: 14px;
  color: ${colors.black};
  text-align: justify;
`;

export const TextWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: "row";
`;

export const UpdateButton = styled.div`
  ${defaultElementBorderRadius()};
  background-color: ${colors.green};
  color: ${colors.white};
  margin-top: 15px;
  padding: 5px 5px 5px;
  font-size: 14px;
`;

export const DeleteButton = styled.div`
  ${defaultElementBorderRadius()};
  background-color: ${colors.red};
  color: ${colors.white};
  margin-top: 15px;
  padding: 5px 5px 5px;
  font-size: 14px;
`;
