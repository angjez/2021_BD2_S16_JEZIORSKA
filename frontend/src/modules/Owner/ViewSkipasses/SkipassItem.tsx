import { Skipass, SkipassEntryType } from "@src/global/types/Skipass.types";
import {
  DeleteButton,
  SkipassBoldText,
  SkipassNameText,
  SkipassText,
  SkipassWrapper,
  TextWrapper,
  UpdateButton,
} from "@src/modules/Owner/ViewSkipasses/SkipassItem.components";
import moment from "moment";
import React, { FunctionComponent } from "react";

interface Props {
  skipass: Skipass;
  deleteSkipass: (id: number) => Promise<void>;
  updateSkipass: (skipass: Skipass) => void;
}

const ViewSkipasses: FunctionComponent<Props> = ({
  skipass,
  deleteSkipass,
  updateSkipass,
}) => {
  return (
    <SkipassWrapper>
      <SkipassNameText>{skipass.name}</SkipassNameText>
      <SkipassText>{skipass.type}</SkipassText>
      <TextWrapper>
        <SkipassBoldText>{skipass.entryType}</SkipassBoldText>
        <SkipassText>
          {skipass.entryType === SkipassEntryType.ENTRIES
            ? skipass.entries
            : skipass.durationHours}
        </SkipassText>
      </TextWrapper>
      <TextWrapper>
        <SkipassBoldText>PRICE</SkipassBoldText>
        <SkipassText>{skipass.price}</SkipassText>
      </TextWrapper>
      <TextWrapper>
        <SkipassBoldText>START DATE</SkipassBoldText>
        <SkipassText>
          {moment(skipass.startsAt).format("DD-MM-YYYY")}
        </SkipassText>
      </TextWrapper>
      <TextWrapper>
        <SkipassBoldText>END DATE</SkipassBoldText>
        <SkipassText>{moment(skipass.endsAt).format("DD-MM-YYYY")}</SkipassText>
      </TextWrapper>
      <TextWrapper>
        <UpdateButton onClick={() => updateSkipass(skipass)}>
          Update
        </UpdateButton>
        <DeleteButton onClick={() => deleteSkipass(skipass.id)}>
          Delete
        </DeleteButton>
      </TextWrapper>
    </SkipassWrapper>
  );
};

export default ViewSkipasses;
