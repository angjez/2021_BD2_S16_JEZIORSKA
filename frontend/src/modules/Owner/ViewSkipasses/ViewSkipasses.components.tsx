import colors from "@src/config/styles/colors";
import styled from "styled-components";

export const ViewSkipassesWrapper = styled.div`
  flex: 1;
  flex-direction: column;
  padding: 25px 25px 25px;
  border-right: 1px solid ${colors.navy};
`;
