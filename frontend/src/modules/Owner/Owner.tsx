import { Skipass } from "@src/global/types/Skipass.types";
import AddUpdateSkipass from "@src/modules/Owner/AddUpdateSkipass";
import {
  AddButton,
  GenerateReportButton,
  ADD_UPDATE_SKIPASS_AREA,
  GridArea,
  OwnerWrapper,
  VIEW_SKIPASSES_AREA,
} from "@src/modules/Owner/Owner.components";
import ViewSkipasses from "@src/modules/Owner/ViewSkipasses";
import axios from "axios";
import React, {
  FunctionComponent,
  useCallback,
  useEffect,
  useState,
} from "react";

const Owner: FunctionComponent = () => {
  const [addUpdateSkipasOpen, setAddUpdateSkipasOpen] = useState(false);
  const [skipasses, setSkipasses] = useState<Skipass[]>([]);

  const getSkipasses = useCallback(async () => {
    try {
      const url = "http://localhost:4000/skipass";
      const response = await axios.get<Skipass[]>(url);
      switch (response.status) {
        case 200:
          setSkipasses(response.data);
      }
    } catch (error) {}
  }, []);
  useEffect(() => {
    getSkipasses();
  }, []);

  const getReport = useCallback(async () => {
    try {
      const url = "http://localhost:4000/report/salesReport";
      axios({
        url: url,
        method: "GET",
        responseType: "blob",
      }).then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "Skipass_sales_report.pdf");
        document.body.appendChild(link);
        link.click();
      });
    } catch (error) {}
  }, []);

  const updateSkipass = useCallback((skipass: Skipass) => {
    setAddUpdateSkipasOpen(true);
    setCurrentlyUpdatedSkipass(skipass);
  }, []);

  const [currentlyUpdatedSkipass, setCurrentlyUpdatedSkipass] =
    useState<Skipass | undefined>();

  return (
    <OwnerWrapper>
      {skipasses.length > 0 && (
        <GridArea area={VIEW_SKIPASSES_AREA}>
          <GenerateReportButton
            onClick={() => {
              getReport();
            }}
          >
            Generate sales report
          </GenerateReportButton>
          <AddButton
            onClick={() => {
              setAddUpdateSkipasOpen(true),
                setCurrentlyUpdatedSkipass(undefined);
            }}
          >
            Add a skipass
          </AddButton>
          <ViewSkipasses
            updateSkipass={updateSkipass}
            refetch={getSkipasses}
            skipasses={skipasses}
          />
        </GridArea>
      )}
      {addUpdateSkipasOpen ? (
        <GridArea area={ADD_UPDATE_SKIPASS_AREA}>
          <AddUpdateSkipass
            currentlyUpdatedSkipass={currentlyUpdatedSkipass}
            refetch={getSkipasses}
            onCancel={() => setAddUpdateSkipasOpen(false)}
          />
        </GridArea>
      ) : null}
    </OwnerWrapper>
  );
};

export default Owner;
