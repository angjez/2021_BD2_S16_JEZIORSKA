import colors from "@src/config/styles/colors";
import { defaultElementBorderRadius } from "@src/config/styles/mixins";
import styled from "styled-components";

export const VIEW_SKIPASSES_AREA = "view-skipasses-area";
export const ADD_UPDATE_SKIPASS_AREA = "add-update-skipass-area";
export const BOX_WIDTH = 396;

export const OwnerWrapper = styled.div`
  flex: 1;
  display: grid;
  grid-template: "${VIEW_SKIPASSES_AREA} ${ADD_UPDATE_SKIPASS_AREA}";
  grid-template-columns: minmax(200px, ${BOX_WIDTH}px) ${BOX_WIDTH}px;
  grid-column-gap: 24px;
  grid-row-gap: 25px;
  padding: 25px 50px 25px;
`;

export const GridArea = styled.div<{ area: string }>`
  grid-area: ${(p) => p.area};
`;

export const AddButton = styled.div`
  ${defaultElementBorderRadius()};
  background-color: ${colors.green};
  text-align: center;
  margin: 5px 50px 5px;
  color: ${colors.white};
  margin-top: 15px;
  padding: 5px 5px 5px;
  font-size: 14px;
`;

export const GenerateReportButton = styled.div`
  ${defaultElementBorderRadius()};
  background-color: ${colors.navy};
  text-align: center;
  margin: 5px 50px 5px;
  color: ${colors.white};
  margin-top: 15px;
  padding: 5px 5px 5px;
  font-size: 14px;
`;
