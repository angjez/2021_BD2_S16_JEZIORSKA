import Select from "react-select";
import styled from "styled-components";

export const AddUpdateSkipassWrapper = styled.div`
  flex-direction: column;
  padding: 25px 25px 25px;
`;

export const StyledSelect = styled(Select)`
  width: 250px;
  font-size: 14px;
  line-height: 1.43;
  letter-spacing: normal;
`;

export const SectionWrapper = styled.div`
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
`;

export const ItemWrapper = styled.div`
  padding: 5px 10px 5px;
`;
