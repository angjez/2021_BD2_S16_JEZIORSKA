import { Skipass } from "@src/global/types/Skipass.types";
import { AddUpdateSkipassWrapper } from "@src/modules/Owner/AddUpdateSkipass/AddUpdateSkipass.components";
import AddUpdateSkipassForm from "@src/modules/Owner/AddUpdateSkipass/AddUpdateSkipassForm";
import React, { FunctionComponent } from "react";

interface Props {
  onCancel: () => void;
  refetch: () => Promise<void>;
  currentlyUpdatedSkipass: Skipass | undefined;
}

const AddUpdateSkipass: FunctionComponent<Props> = ({
  onCancel,
  refetch,
  currentlyUpdatedSkipass,
}) => {
  return (
    <AddUpdateSkipassWrapper>
      <AddUpdateSkipassForm
        currentlyUpdatedSkipass={currentlyUpdatedSkipass}
        onCancel={onCancel}
        refetch={refetch}
      />
    </AddUpdateSkipassWrapper>
  );
};

export default AddUpdateSkipass;
