import {
  Skipass,
  SkipassEntryType,
  SkipassType,
} from "@src/global/types/Skipass.types";

export interface AddUpdateSkipassFormValues {
  name: string;
  type: SkipassType;
  entryType: SkipassEntryType;
  durationHours: number;
  entries: number;
  price: number;
  startsAt: Date;
  endsAt: Date;
}

export const getInitalValues = (
  currentlyUpdatedSkipass: Skipass | undefined,
): AddUpdateSkipassFormValues => ({
  name: currentlyUpdatedSkipass ? currentlyUpdatedSkipass.name : "",
  type: currentlyUpdatedSkipass
    ? currentlyUpdatedSkipass.type
    : SkipassType.STANDARD,
  entryType: currentlyUpdatedSkipass
    ? currentlyUpdatedSkipass.entryType
    : SkipassEntryType.ENTRIES,
  durationHours: currentlyUpdatedSkipass?.durationHours
    ? currentlyUpdatedSkipass.durationHours
    : 0,
  entries: currentlyUpdatedSkipass?.entries
    ? currentlyUpdatedSkipass.entries
    : 0,
  price: currentlyUpdatedSkipass ? currentlyUpdatedSkipass.price : 0,
  startsAt: currentlyUpdatedSkipass?.startsAt
    ? currentlyUpdatedSkipass.startsAt
    : new Date(),
  endsAt: currentlyUpdatedSkipass?.endsAt
    ? currentlyUpdatedSkipass.endsAt
    : new Date(),
});
