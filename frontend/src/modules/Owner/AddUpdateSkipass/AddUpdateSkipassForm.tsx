import {
  Skipass,
  SkipassEntryType,
  SkipassType,
  UpdateSkipass,
} from "@src/global/types/Skipass.types";
import AddSkipassFormContent, {
  SelectOptionEntryType,
  SelectOptionType,
} from "@src/modules/Owner/AddUpdateSkipass/AddUpdateSkipassFormContent";
import {
  AddUpdateSkipassFormValues,
  getInitalValues,
} from "@src/modules/Owner/AddUpdateSkipass/form";
import axios from "axios";
import { Formik } from "formik";
import React, { FunctionComponent, useCallback } from "react";
import { toast } from "react-toastify";

interface Props {
  onCancel: () => void;
  refetch: () => Promise<void>;
  currentlyUpdatedSkipass: Skipass | undefined;
}

const types: SelectOptionType<any>[] = [
  { value: SkipassType.JUNIOR, label: "Junior" },
  { value: SkipassType.STANDARD, label: "Standard" },
  { value: SkipassType.SENIOR, label: "Senior" },
];

const entryTypes: SelectOptionEntryType<any>[] = [
  { value: SkipassEntryType.TIME, label: "Time" },
  { value: SkipassEntryType.ENTRIES, label: "Entries" },
];

const AddUpdateSkipassForm: FunctionComponent<Props> = ({
  onCancel,
  refetch,
  currentlyUpdatedSkipass,
}) => {
  const onSubmit = useCallback(async (data: AddUpdateSkipassFormValues) => {
    try {
      const url = "http://localhost:4000/skipass";
      const response =
        currentlyUpdatedSkipass !== undefined
          ? await axios.put<UpdateSkipass>(
              `${url}/${currentlyUpdatedSkipass.id}`,
              {
                name: data.name,
                price: data.price,
                startsAt: data.startsAt,
                endsAt: data.endsAt,
              },
            )
          : await axios.post<AddUpdateSkipassFormValues>(url, data);
      switch (response.status) {
        // 400 - bad request (form error)
        case 400:
          toast.error(
            "Something went wrong. Please check your input and try again later.",
          );
          break;
        case 201:
          break;
      }
    } catch (error) {
    } finally {
      await refetch();
      onCancel();
    }
  }, []);
  return (
    <Formik
      initialValues={getInitalValues(currentlyUpdatedSkipass)}
      onSubmit={onSubmit}
    >
      {(formProps) => (
        <AddSkipassFormContent
          {...formProps}
          isEditMode={currentlyUpdatedSkipass !== undefined}
          onCancel={onCancel}
          types={types}
          entryTypes={entryTypes}
        />
      )}
    </Formik>
  );
};

export default AddUpdateSkipassForm;
