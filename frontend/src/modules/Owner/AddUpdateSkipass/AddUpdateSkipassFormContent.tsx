import { SkipassEntryType, SkipassType } from "@src/global/types/Skipass.types";
import Button from "@src/modules/components/Form/Button";
import InputField from "@src/modules/components/Form/Input/InputField";
import Label from "@src/modules/components/Form/Input/Label";
import {
  ItemWrapper,
  SectionWrapper,
  StyledSelect,
} from "@src/modules/Owner/AddUpdateSkipass/AddUpdateSkipass.components";
import { AddUpdateSkipassFormValues } from "@src/modules/Owner/AddUpdateSkipass/form";
import { Form, FormikProps } from "formik";
import React, { FunctionComponent } from "react";
import DatePicker from "react-date-picker";

export interface SelectOptionEntryType<SkipassEntryType> {
  label: string;
  value: SkipassEntryType;
}

export interface SelectOptionType<SkipassType> {
  label: string;
  value: SkipassType;
}

type Props = FormikProps<AddUpdateSkipassFormValues>;
type AddUpdateSkipassFormContentProps = {
  isEditMode: boolean;
  onCancel: () => void;
  types: SelectOptionType<any>[];
  entryTypes: SelectOptionEntryType<any>[];
};

const AddUpdateSkipassFormContent: FunctionComponent<
  Props & AddUpdateSkipassFormContentProps
> = ({
  isEditMode,
  setFieldValue,
  submitForm,
  values,
  types,
  entryTypes,
  onCancel,
}) => {
  return (
    <Form>
      <Label>Name</Label>
      <InputField name="name" sizeType="small" />
      {!isEditMode && (
        <>
          <Label>Type</Label>
          <StyledSelect
            name="type"
            placeholder="Select a skipass type"
            options={types}
            onChange={(option: SelectOptionType<SkipassType>) =>
              setFieldValue("type", option.value)
            }
          />
          <Label>Entry type</Label>
          <StyledSelect
            name="entryType"
            placeholder="Select an entry type"
            options={entryTypes}
            onChange={(option: SelectOptionEntryType<SkipassEntryType>) =>
              setFieldValue("entryType", option.value)
            }
          />{" "}
        </>
      )}
      {values.entryType && !isEditMode && (
        <>
          <Label>
            {values.entryType === SkipassEntryType.ENTRIES
              ? "Entries"
              : "Duration (hours)"}
          </Label>
          <InputField
            name={
              values.entryType === SkipassEntryType.ENTRIES
                ? "entries"
                : "durationHours"
            }
            sizeType="small"
          />
        </>
      )}
      <Label>Price</Label>
      <InputField name="price" sizeType="small" />
      <Label>Starts at</Label>
      <ItemWrapper>
        <DatePicker
          name="startsAt"
          value={values.startsAt}
          onChange={(date) => setFieldValue("startsAt", date)}
        />
      </ItemWrapper>
      <Label>Ends at</Label>
      <ItemWrapper>
        <DatePicker
          name="endsAt"
          value={values.endsAt}
          onChange={(date) => setFieldValue("endsAt", date)}
        />
      </ItemWrapper>
      <SectionWrapper>
        <Button type="submit">Submit</Button>
        <Button type="button" onClick={onCancel}>
          Cancel
        </Button>
      </SectionWrapper>
    </Form>
  );
};

export default AddUpdateSkipassFormContent;
