import { FunctionComponent } from "react";
import { ChakraProvider } from "@chakra-ui/react";

import { MainFlex } from "@src/modules/Cashier/Cashier.components";
import DepositReturn from "@src/modules/Cashier/DepositReturn/DepositReturn";
import SkipassSell from "src/modules/Cashier/SkipassSell/SkipassSell";

const Cashier: FunctionComponent = () => {
  return (
    <ChakraProvider>
      <MainFlex>
        <DepositReturn />
        <SkipassSell />
      </MainFlex>
    </ChakraProvider>
  );
};

export default Cashier;
