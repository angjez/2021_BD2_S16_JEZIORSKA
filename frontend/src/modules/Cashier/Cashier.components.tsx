import { Box } from "@chakra-ui/layout";
import { Text } from "@chakra-ui/react";
import styled from "styled-components";

export const MainFlex = styled(Box)`
  margin-right: 20px;
  margin-left: 20px;
  margin-top: 20px;
  max-height: 600px;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

export const HeaderText = styled(Text)`
  font-size: 28px;
`;
