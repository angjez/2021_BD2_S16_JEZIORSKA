import { Skeleton } from "@chakra-ui/skeleton";
import { Table, Tbody, Th, Thead, Tr } from "@chakra-ui/table";
import { SkipassEntryType } from "@src/enums/skipass-entry-type.enum";
import { BoughtSkipass } from "@src/interfaces/bought-skipass.interface";
import { Skier } from "@src/interfaces/skier.interface";
import { DespositEntry } from "@src/modules/Cashier/DepositReturn/DepositEntry";
import {
  ContainerBox,
  SearchInput,
  TableBox,
} from "@src/modules/Cashier/DepositReturn/DepositReturn.components";
import axios from "axios";
import React, { FunctionComponent, useEffect, useState } from "react";
import { toast } from "react-toastify";

interface DepositData {
  boughtSkipassId: number;
  entryType: SkipassEntryType;
  skierName: string;
}

const DepositReturn: FunctionComponent = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [depositData, setDepositData] = useState<DepositData[]>([]);
  const [filteredDepositData, setFilteredDepositData] = useState<DepositData[]>(
    [],
  );

  const unreturnedBoughtSkipassesURL =
    "http://localhost:4000/bought-skipass/unreturned";
  const skiersURL = "http://localhost:4000/skier";

  const fetchDepositData = async () => {
    setIsLoading(true);
    try {
      const currentSkipassesReponse = await axios.get<BoughtSkipass[]>(
        unreturnedBoughtSkipassesURL,
      );
      const skiersReponse = await axios.get<Skier[]>(skiersURL);
      if (
        currentSkipassesReponse.status === 200 &&
        skiersReponse.status === 200
      ) {
        const newDepositData: DepositData[] = currentSkipassesReponse.data.map(
          (b) => {
            const skierLastName =
              skiersReponse.data.find((s) => s.id === b.skierId)?.lastName ||
              "unknown";
            return {
              boughtSkipassId: b.id,
              entryType: b.entryType,
              skierName: skierLastName,
            };
          },
        );
        setDepositData(newDepositData);
        setFilteredDepositData(newDepositData);
        setIsLoading(false);
      } else {
        toast.error("Failed to fetch deposits");
      }
    } catch (e) {
      toast.error("Failed to fetch deposits: ".concat(e));
    }
  };

  const handleSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const searchValue = e.target.value.toLowerCase();
    searchValue
      ? setFilteredDepositData(
          depositData.filter(
            (d) =>
              d.skierName.toLowerCase().includes(searchValue) ||
              d.boughtSkipassId.toString().includes(searchValue),
          ),
        )
      : setFilteredDepositData(depositData);
  };

  const depositEntries = () =>
    filteredDepositData.map((d) => {
      return (
        <DespositEntry
          key={d.boughtSkipassId}
          skipassId={d.boughtSkipassId}
          entryType={d.entryType}
          name={d.skierName}
          refreshHandler={fetchDepositData}
        />
      );
    });

  useEffect(() => {
    fetchDepositData();
  }, []);

  return (
    <ContainerBox borderRadius="md">
      <Skeleton isLoaded={!isLoading}>
        <TableBox>
          <Table style={{ overflowY: "scroll" }}>
            <Thead>
              <Tr>
                <Th>Skipass ID</Th>
                <Th>Entry type</Th>
                <Th>Skier</Th>
                <Th>Deposit</Th>
              </Tr>
            </Thead>
            <Tbody style={{ overflowY: "scroll" }}>{depositEntries()}</Tbody>
          </Table>
        </TableBox>
        <SearchInput onChange={handleSearchInput} />
      </Skeleton>
    </ContainerBox>
  );
};

export default DepositReturn;
