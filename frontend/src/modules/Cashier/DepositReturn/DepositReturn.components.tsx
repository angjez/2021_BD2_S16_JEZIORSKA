import { Box, Input } from "@chakra-ui/react";
import styled from "styled-components";

export const ContainerBox = styled(Box)`
  flex: 3;
  border-width: 1px;
  margin-right: 10px;
  display: flex;
  flex-direction: column;
  padding: 10px;
  max-width: 1300px;
`;

export const TableBox = styled(Box)`
  height: 400px;
  overflow-y: scroll;
`;

export const SearchInput = styled(Input).attrs({
  placeholder: "Search...",
})`
  margin-top: 18px;
`;
