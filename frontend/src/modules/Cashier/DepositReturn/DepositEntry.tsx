import { Tr, Td } from "@chakra-ui/table";
import axios from "axios";
import { toast } from "react-toastify";
import { FunctionComponent } from "react";
import { Button } from "@chakra-ui/react";
import { SkipassEntryType } from "@src/global/types/Skipass.types";

interface DepositEntryProps {
  skipassId: number;
  entryType: SkipassEntryType;
  name: string;
  refreshHandler: () => void;
}

export const DespositEntry: FunctionComponent<DepositEntryProps> = ({
  skipassId,
  entryType,
  name,
  refreshHandler,
}: DepositEntryProps) => {
  const boughtSkipassURL = "http://localhost:4000/bought-skipass/";

  const returnDeposit = async () => {
    try {
      const returnResponse = await axios.put(
        boughtSkipassURL.concat(skipassId.toString()),
        { wasReturned: true },
      );
      if (returnResponse.status === 200) {
        toast.info(
          "Successfully returned deposit for skipass: ".concat(
            skipassId.toString(),
          ),
        );
        await refreshHandler();
      }
    } catch (e) {
      toast.error("Failed to return deposit! Error: ".concat(e));
    }
  };

  return (
    <Tr>
      <Td>{skipassId}</Td>
      <Td>{entryType}</Td>
      <Td>{name}</Td>
      <Td>
        <Button onClick={returnDeposit} size="sm">
          Return
        </Button>
      </Td>
    </Tr>
  );
};
