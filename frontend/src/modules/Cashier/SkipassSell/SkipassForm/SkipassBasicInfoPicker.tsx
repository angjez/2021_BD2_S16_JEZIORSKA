import { FunctionComponent } from "react";
import { VStack, Text } from "@chakra-ui/react";
import DatePicker from "react-date-picker";
import { Input } from "@chakra-ui/input";
import { HorizontalStack } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassForm.components";

interface SkipassBasicInfoPickerProps {
  firstNameHandler: (fName: string) => void;
  lastNameHandler: (lName: string) => void;
  bDayHandler: (bDay: Date) => void;
  bDay: Date;
}

export const SkipassBasicInfoPicker: FunctionComponent<SkipassBasicInfoPickerProps> =
  ({
    firstNameHandler,
    lastNameHandler,
    bDayHandler,
    bDay,
  }: SkipassBasicInfoPickerProps) => {
    return (
      <HorizontalStack>
        <VStack flex={1}>
          <Text>Name</Text>
          <Input
            onChange={(e) => firstNameHandler(e.target.value)}
            placeholder="Name..."
          />
        </VStack>
        <VStack flex={1}>
          <Text>Surname</Text>
          <Input
            onChange={(e) => lastNameHandler(e.target.value)}
            placeholder="Surname..."
          />
        </VStack>
        <VStack flex={1}>
          <Text>Date of birth</Text>
          <DatePicker value={bDay} onChange={(date) => bDayHandler(date)} />
        </VStack>
      </HorizontalStack>
    );
  };
