import { FunctionComponent, useState } from "react";
import { Stat, StatLabel, StatNumber, Button } from "@chakra-ui/react";
import { HorizontalStack } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassForm.components";

export interface SkipassSellProps {
  currentPrice: number;
  sellHandler: () => void;
}

export const SkipassSellComponent: FunctionComponent<SkipassSellProps> = ({
  currentPrice,
  sellHandler,
}) => {
  const [buttonLoading, setButtonLoading] = useState<boolean>(false);
  const onSell = async () => {
    setButtonLoading(true);
    setTimeout(() => setButtonLoading(false), 1000);
    await sellHandler();
  };

  return (
    <HorizontalStack style={{ marginTop: "20px" }}>
      <Stat flex={1}>
        <StatLabel>Skipass price:</StatLabel>
        <StatNumber>{currentPrice.toString()}</StatNumber>
      </Stat>
      <Button
        isLoading={buttonLoading}
        onClick={onSell}
        colorScheme="green"
        flex={2}
      >
        Sell skipass
      </Button>
    </HorizontalStack>
  );
};
