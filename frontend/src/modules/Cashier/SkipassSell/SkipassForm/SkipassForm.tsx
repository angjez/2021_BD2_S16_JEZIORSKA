import { FunctionComponent, useEffect, useState } from "react";
import { Divider } from "@chakra-ui/layout";
import { FormControl } from "@chakra-ui/form-control";
import { FormContainer } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassForm.components";
import { SkipassBasicInfoPicker } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassBasicInfoPicker";
import { SkipassTypeInfoPicker } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassTypeInfoPicker";
import { SkipassEntriesPicker } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassEntriesPicker";
import { SkipassDatePicker } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassDatePicker";
import { SkipassSellComponent } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassSellComponent";
import { SkipassEntryType } from "@src/enums/skipass-entry-type.enum";
import { NewSkier } from "@src/interfaces/new-skier.interface";
import { NewBoughtSkipass } from "@src/interfaces/new-bought-skipass.interface";
import Skipass from "@src/interfaces/skipass.interface";

interface SkipassFormProps {
  preset: Skipass;
  postNewSkipassHandler: (
    newSkier: NewSkier,
    newBoughtSkipass: NewBoughtSkipass,
  ) => void;
}

export const SkipassForm: FunctionComponent<SkipassFormProps> = ({
  preset,
  postNewSkipassHandler,
}: SkipassFormProps) => {
  const { TIME, ENTRIES } = SkipassEntryType;

  const [firstName, setFirstName] = useState<string>("");
  const [lastName, setLastName] = useState<string>("");
  const [bday, setBday] = useState<Date>(new Date());

  const [startsAt, setStartsAt] = useState<Date>(new Date());
  const [endsAt, setEndsAt] = useState<Date>(new Date());

  const { id, type, entryType, durationHours, entries, price } = preset;

  const handleSell = async () => {
    const newSkier: NewSkier = { firstName, lastName, dob: bday };
    let newBoughtSkipass: NewBoughtSkipass = {
      entryType,
      skierId: -1,
      skipassId: id,
    };
    switch (entryType) {
      case TIME:
        newBoughtSkipass = {
          ...newBoughtSkipass,
          startsAt,
          endsAt,
          entries: undefined,
        };
        postNewSkipassHandler(newSkier, newBoughtSkipass);
        break;
      case ENTRIES:
        newBoughtSkipass = {
          ...newBoughtSkipass,
          entries,
          startsAt: undefined,
          endsAt: undefined,
        };
        postNewSkipassHandler(newSkier, newBoughtSkipass);
        break;
    }
  };

  useEffect(() => {
    const newEndsAt = new Date(startsAt);
    if (durationHours) {
      newEndsAt.setHours(startsAt.getHours() + durationHours);
      setEndsAt(newEndsAt);
    }
  }, [startsAt]);

  return (
    <FormContainer>
      <FormControl isDisabled={preset ? false : true}>
        <SkipassBasicInfoPicker
          firstNameHandler={setFirstName}
          lastNameHandler={setLastName}
          bDayHandler={setBday}
          bDay={bday}
        />

        <Divider />

        <SkipassTypeInfoPicker skipassType={type} entryType={entryType} />

        <Divider />

        {entryType === TIME && (
          <>
            <SkipassDatePicker
              currentDate={startsAt}
              newDateHandler={setStartsAt}
            />
            <SkipassDatePicker
              currentDate={endsAt}
              showsEndDate
              newDateHandler={setEndsAt}
            />
          </>
        )}
        {entryType === ENTRIES && entries && (
          <SkipassEntriesPicker entries={entries} />
        )}

        <Divider />

        <SkipassSellComponent sellHandler={handleSell} currentPrice={price} />
      </FormControl>
    </FormContainer>
  );
};
