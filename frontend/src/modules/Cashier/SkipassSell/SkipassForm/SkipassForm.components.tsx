import styled from "styled-components";
import { Box, HStack } from "@chakra-ui/react";

export const FormContainer = styled(Box)`
  margin-top: 10px;
`;

export const HorizontalStack = styled(HStack)`
  display: flex;
  margin-top: 10px;
  margin-bottom: 10px;
  justify-content: space-evenly;
`;
