import { FunctionComponent } from "react";
import DatePicker from "react-date-picker";
import {
  Text,
  NumberInput,
  NumberIncrementStepper,
  NumberDecrementStepper,
  NumberInputField,
  NumberInputStepper,
} from "@chakra-ui/react";
import { HorizontalStack } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassForm.components";

export interface SkipassDatePickerProps {
  showsEndDate?: boolean;
  newDateHandler: (date: Date) => void;
  currentDate: Date;
}

export const SkipassDatePicker: FunctionComponent<SkipassDatePickerProps> = ({
  showsEndDate = false,
  newDateHandler,
  currentDate,
}: SkipassDatePickerProps) => {
  const changeHours = (hours?: number) => {
    if (hours) {
      const newDate = new Date(currentDate);
      newDate.setHours(hours);
      newDateHandler(newDate);
    }
  };
  const changeMinutes = (minutes?: number) => {
    if (minutes) {
      const newDate = new Date(currentDate);
      newDate.setMinutes(minutes);
      newDateHandler(newDate);
    }
  };
  const changeDate = (date?: Date) => {
    if (date) {
      const newDate = new Date(currentDate);
      newDate.setFullYear(date.getFullYear());
      newDate.setMonth(date.getMonth());
      newDate.setDate(date.getDate());
      newDateHandler(newDate);
    }
  };

  return (
    <HorizontalStack>
      <Text flex={1}>{showsEndDate ? "Ends at" : "Starts at"}</Text>
      <DatePicker
        disabled={showsEndDate}
        value={currentDate}
        onChange={(date) => changeDate(date)}
        required
        clearIcon={null}
      />

      <HorizontalStack flex={1}>
        <NumberInput
          isDisabled={showsEndDate}
          min={0}
          max={23}
          value={currentDate.getHours()}
          onChange={(s) => changeHours(parseInt(s))}
        >
          <NumberInputField />
          <NumberInputStepper>
            <NumberIncrementStepper />
            <NumberDecrementStepper />
          </NumberInputStepper>
        </NumberInput>

        <Text>:</Text>

        <NumberInput
          isDisabled={showsEndDate}
          min={0}
          max={59}
          value={currentDate.getMinutes()}
          onChange={(s) => changeMinutes(parseInt(s))}
        >
          <NumberInputField />
          <NumberInputStepper>
            <NumberIncrementStepper />
            <NumberDecrementStepper />
          </NumberInputStepper>
        </NumberInput>
      </HorizontalStack>
    </HorizontalStack>
  );
};
