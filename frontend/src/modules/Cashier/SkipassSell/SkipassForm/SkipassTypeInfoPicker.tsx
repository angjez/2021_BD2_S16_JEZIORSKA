import { FunctionComponent } from "react";
import { Text, Center } from "@chakra-ui/react";
import { Divider } from "@chakra-ui/layout";
import { HorizontalStack } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassForm.components";

interface SkipassTypeInfoPickerProps {
  skipassType: string;
  entryType: string;
}

export const SkipassTypeInfoPicker: FunctionComponent<SkipassTypeInfoPickerProps> =
  ({ skipassType, entryType }: SkipassTypeInfoPickerProps) => {
    return (
      <HorizontalStack>
        <HorizontalStack>
          <Text flex={3}>Skipass type:</Text>
          <Text flex={1}>{skipassType}</Text>
        </HorizontalStack>

        <Center height="50px">
          <Divider orientation="vertical" />
        </Center>

        <HorizontalStack>
          <Text flex={3}>Entry type:</Text>
          <Text flex={1}>{entryType}</Text>
        </HorizontalStack>
      </HorizontalStack>
    );
  };
