import { FunctionComponent } from "react";
import {
  NumberInput,
  NumberIncrementStepper,
  NumberDecrementStepper,
  NumberInputField,
  NumberInputStepper,
  Text,
} from "@chakra-ui/react";
import { HorizontalStack } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassForm.components";

interface SkipassEntriesPickerProps {
  entries: number;
}

export const SkipassEntriesPicker: FunctionComponent<SkipassEntriesPickerProps> =
  ({ entries }: SkipassEntriesPickerProps) => {
    return (
      <HorizontalStack style={{ marginRight: "60px", marginLeft: "60px" }}>
        <Text flex={2}>Entries:</Text>
        <NumberInput isDisabled={true} value={entries} min={1} flex={4}>
          <NumberInputField placeholder="Entries count..." />
          <NumberInputStepper>
            <NumberIncrementStepper />
            <NumberDecrementStepper />
          </NumberInputStepper>
        </NumberInput>
      </HorizontalStack>
    );
  };
