import { Divider, Select, Skeleton } from "@chakra-ui/react";
import axios from "axios";
import { toast } from "react-toastify";
import { FunctionComponent, useEffect, useState } from "react";
import { SkipassForm } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassForm";
import { SellContainer } from "@src/modules/Cashier/SkipassSell/SkipassSell.components";
import { HeaderText } from "@src/modules/Cashier/Cashier.components";
import { HorizontalStack } from "@src/modules/Cashier/SkipassSell/SkipassForm/SkipassForm.components";
import Skipass from "@src/interfaces/skipass.interface";
import { NewBoughtSkipass } from "@src/interfaces/new-bought-skipass.interface";
import { NewSkier } from "@src/interfaces/new-skier.interface";
import { Skier } from "@src/interfaces/skier.interface";

const SkipassSell: FunctionComponent = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [skipassPresets, setSkipassPresets] = useState<Skipass[]>([]);
  const [selectedPreset, setSelectedPreset] = useState<Skipass>();

  const currentSkipassesURL = "http://localhost:4000/skipass/current";
  const skierURL = "http://localhost:4000/skier";
  const boughtSkipassURL = "http://localhost:4000/bought-skipass";

  const postNewData = async (
    newSkier: NewSkier,
    newBoughtSkipass: NewBoughtSkipass,
  ) => {
    try {
      const skierResponse = await axios.post<Skier>(skierURL, newSkier);
      if (skierResponse.status === 201) {
        const newSkier = skierResponse.data;
        newBoughtSkipass = { ...newBoughtSkipass, skierId: newSkier.id };

        const boughtSkipassResponse = await axios.post<NewBoughtSkipass>(
          boughtSkipassURL,
          newBoughtSkipass,
        );
        if (boughtSkipassResponse.status === 201) {
          toast.info("Successfully sold skipass!");
          setTimeout(() => window.location.reload(), 1500);
        } else {
          await axios.delete(
            skierURL.concat("/").concat(newSkier.id.toString()),
          );
          toast.error(
            "An error occured during BoughtSkipass creation. Try again later.",
          );
        }
      } else {
        toast.error("An error occured during Skier creation. Try again later.");
      }
    } catch (e) {
      toast.error(
        "Error: "
          .concat(e)
          .concat(". Check if all fields were filled correctly."),
      );
    }
  };

  const fetchSkipassPresets = async () => {
    setIsLoading(true);
    try {
      const response = await axios.get(currentSkipassesURL);
      if (response.status === 200) {
        setSkipassPresets(response.data);
        setIsLoading(false);
      } else {
        toast.error("Failed to fetch skipasses. Try refreshing page.");
      }
    } catch (e) {
      toast.error("Failed to fetch skipasses. Try refreshing page. ".concat(e));
    }
  };

  useEffect(() => {
    fetchSkipassPresets();
  }, []);

  return (
    <SellContainer borderRadius="md">
      <Skeleton style={{ borderRadius: 5 }} isLoaded={!isLoading}>
        <HorizontalStack style={{ paddingRight: 80, paddingLeft: 80 }}>
          <HeaderText flex={1} fontSize="xl">
            Skipass:
          </HeaderText>

          <Select
            placeholder="Select skipass to sell..."
            flex={2}
            value={selectedPreset?.name}
            onChange={(e) => {
              setSelectedPreset(
                skipassPresets.find((p) => p.name === e.target.value),
              );
            }}
          >
            {skipassPresets.map((p) => {
              return (
                <option key={p.id} value={p.name}>
                  {p.name}
                </option>
              );
            })}
          </Select>
        </HorizontalStack>

        <Divider height="10px" />
        {selectedPreset && (
          <SkipassForm
            preset={selectedPreset}
            postNewSkipassHandler={postNewData}
          />
        )}

        <Divider height="10px" />
      </Skeleton>
    </SellContainer>
  );
};

export default SkipassSell;
