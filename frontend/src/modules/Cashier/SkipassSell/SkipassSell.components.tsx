import styled from "styled-components";
import { Box } from "@chakra-ui/react";

export const SellContainer = styled(Box)`
  flex: 5;
  border-width: 1px;
  margin-left: 10px;
  padding: 10px;
  text-align: center;
  max-width: 1300px;
`;
