import colors from "@src/config/styles/colors";
import { Link } from "react-router-dom";
import styled from "styled-components";

export const Wrapper = styled.div`
  min-height: 64px;
  display: flex;
`;

export const Option = styled(Link)<{ isactive: string }>`
  text-decoration: none;
  display: flex;
  height: 100%;
  align-items: center;
  padding: 0 24px;
  font-size: 14px;
  line-height: 1.71;
  color: ${colors.white};
  cursor: pointer;
  padding-bottom: 20px;
  padding-top: 21px;

  &:hover {
    color: ${colors.lightBlue};
  }

  ${(props) =>
    props.isactive === "true" &&
    `color: white; font-weight: 600; line-height: 1.43;
 border-bottom: 2px solid white; padding-bottom: 18px;padding-top: 24px;
 background: rgba(255, 255, 255, 0.1); 
`}
`;
