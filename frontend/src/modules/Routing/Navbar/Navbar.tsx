import { Wrapper } from "@src/modules/components/Tabs/Tabs.components";
import NavbarItem from "@src/modules/Routing/Navbar/NavbarItem";
import routes from "@src/modules/Routing/routes";
import React, { FunctionComponent } from "react";

const Navbar: FunctionComponent = ({ children }) => (
  <>
    <Wrapper>
      {routes.map((item) => (
        <NavbarItem key={item.path} item={item} />
      ))}
    </Wrapper>
    {children}
  </>
);

export default Navbar;
