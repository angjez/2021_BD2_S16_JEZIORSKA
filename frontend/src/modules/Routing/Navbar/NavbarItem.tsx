import NavbarItemContent from "@src/modules/Routing/Navbar/NavbarItemContent";
import routes, { RouteType } from "@src/modules/Routing/routes";
import React, { FunctionComponent, useMemo } from "react";

interface Props {
  item: RouteType;
}

const NavbarItem: FunctionComponent<Props> = ({ item }) => {
  const route = useMemo(
    () => routes.find((route) => route.path === item.path),
    [item],
  );

  const title = useMemo(
    () => (item.title ? item.title : route ? route.title : ""),
    [item, route],
  );

  return <NavbarItemContent title={title} path={item.path} />;
};

export default NavbarItem;
