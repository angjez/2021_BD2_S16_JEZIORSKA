import { Option } from "@src/modules/Routing/Navbar/Navbar.components";
import React, { FunctionComponent, useMemo } from "react";
import { useLocation } from "react-router";

interface Props {
  title: string;
  path: string;
}

const NavbarItemContent: FunctionComponent<Props> = ({ title, path }) => {
  const location = useLocation();

  const isActive = useMemo(
    () => location.pathname.startsWith(path),
    [location.pathname, path],
  );

  return (
    <Option to={path} key={path} isactive={isActive.toString()}>
      {title}
    </Option>
  );
};

export default NavbarItemContent;
