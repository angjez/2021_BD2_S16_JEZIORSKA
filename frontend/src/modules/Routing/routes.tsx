import pathNames from "@src/config/pathNames/pathNames";
import { RouteComponentProps } from "react-router-dom";

export interface RouteType {
  path: string;
  title: string;
  component: () => Promise<{
    default:
      | React.ComponentType<RouteComponentProps<undefined>>
      | React.ComponentType<unknown>;
  }>;
}

const routes: RouteType[] = [
  {
    path: pathNames.cashier,
    title: "Cashier",
    component: () => import("@src/modules/Cashier"),
  },
  {
    path: pathNames.skier,
    title: "Skier",
    component: () => import("@src/modules/Skier"),
  },
  {
    path: pathNames.owner,
    title: "Owner",
    component: () => import("@src/modules/Owner"),
  },
  {
    path: pathNames.manager,
    title: "Manager",
    component: () => import("@src/modules/Manager"),
  },
];

export default routes;
