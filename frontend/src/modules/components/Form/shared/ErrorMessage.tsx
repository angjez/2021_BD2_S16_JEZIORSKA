import colors from "@src/config/styles/colors";
import styled from "styled-components";

export const ERROR_MARGIN = 8;
export const ERROR_HEIGHT = 15;

const ErrorMessage = styled.div`
  margin-top: ${ERROR_MARGIN}px;
  line-height: 1.33;
  font-size: 12px;
  color: ${colors.red};
`;

export default ErrorMessage;
