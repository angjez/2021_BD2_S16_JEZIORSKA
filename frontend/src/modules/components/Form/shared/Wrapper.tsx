import { INPUT_SHOW_ERROR_MARGIN } from "@src/modules/components/Form/Input/Input.components";
import {
  ERROR_HEIGHT,
  ERROR_MARGIN,
} from "@src/modules/components/Form/shared/ErrorMessage";
import styled, { css } from "styled-components";

interface WrapperProps {
  isError?: boolean;
  showError?: boolean;
  errorLinesNo?: number;
}

const Wrapper = styled.div<WrapperProps>`
  position: relative;
  ${(p) =>
    p.showError &&
    css`
      margin-bottom: ${p.isError
        ? INPUT_SHOW_ERROR_MARGIN
        : (p.errorLinesNo || 1) * ERROR_HEIGHT +
          ERROR_MARGIN +
          INPUT_SHOW_ERROR_MARGIN}px;
    `};
`;

export default Wrapper;
