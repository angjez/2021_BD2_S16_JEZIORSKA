import {
  ButtonWrapper,
  StyledButtonProps,
} from "@src/modules/components/Form/Button/Button.components";
import React, { ComponentPropsWithoutRef, FunctionComponent } from "react";

export type ButtonProps = ComponentPropsWithoutRef<"button"> &
  StyledButtonProps;

const Button: FunctionComponent<ButtonProps> = ({
  children,
  active = true,
  ...props
}) => (
  <ButtonWrapper active={active} disabled={!active} {...props}>
    {children}
  </ButtonWrapper>
);

export default Button;
