import colors from "@src/config/styles/colors";
import styled from "styled-components";

const NORMAL_BUTTON_WIDTH = 250;
const BUTTON_HEIGHT = 40;

export interface StyledButtonProps {
  active?: boolean;
  cancel?: boolean;
}

export const ButtonWrapper = styled.button<StyledButtonProps>`
  flex-direction: row;
  border-radius: 4px;
  height: ${BUTTON_HEIGHT}px;
  width: ${NORMAL_BUTTON_WIDTH}px;
  margin: 25px 25px 0px;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  color: ${colors.white};
  text-align: center;

  background-color: ${(p) =>
    p.active ? (p.cancel ? colors.lightBlue : colors.navy) : colors.middleGrey};
`;
