import { Input, InputProps } from "@src/modules/components/Form/Input/Input";
import { Field, FieldAttributes, FieldProps } from "formik";
import React, { FunctionComponent } from "react";

export type InputFieldProps = FieldAttributes<{
  secondIcon?: string;
  onSecondIconClick?: () => void;
  isSearch?: boolean;
}> &
  Pick<
    InputProps,
    | "placeholder"
    | "type"
    | "isDisabled"
    | "icon"
    | "onFocus"
    | "iconPosition"
    | "onIconClick"
    | "addon"
    | "sizeType"
    | "min"
    | "max"
    | "showError"
  >;

const InputField: FunctionComponent<InputFieldProps> = ({
  placeholder,
  type,
  isDisabled,
  isSearch = false,
  icon,
  secondIcon,
  iconPosition,
  onIconClick,
  onSecondIconClick,
  onFocus,
  addon,
  sizeType,
  min,
  max,
  showError,
  ...fieldProps
}) => (
  <Field {...fieldProps}>
    {({ field, meta }: FieldProps) => (
      <>
        <Input
          {...field}
          min={min}
          max={max}
          placeholder={placeholder}
          type={type}
          isDisabled={isDisabled}
          isSearch={isSearch}
          icon={icon}
          secondIcon={secondIcon}
          sizeType={sizeType}
          iconPosition={iconPosition}
          onIconClick={onIconClick}
          onSecondIconClick={onSecondIconClick}
          onFocus={onFocus}
          addon={addon}
          showError={showError}
          error={meta.touched && meta.error ? meta.error : undefined}
        />
      </>
    )}
  </Field>
);

export default InputField;
