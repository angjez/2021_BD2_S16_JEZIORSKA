import colors from "@src/config/styles/colors";
import styled, { css } from "styled-components";

export type InputSize = "small" | "normal" | "medium" | "unset";

interface StyledInputProps {
  isError?: boolean;
  isIcon?: boolean;
  iconPosition?: IconPosition;
  isSearch?: boolean;
}
interface WrapperProps {
  isError?: boolean;
  sizeType?: InputSize;
  isSearch?: boolean;
}
export const SMALL_INPUT_WIDTH = 250;
export const MEDIUM_INPUT_WIDTH = 360;
export const NORMAL_INPUT_WIDTH = 468;

export const LARGE_INPUT_VERTICAL_PADDING = 10.5;
export const NORMAL_INPUT_VERTICAL_PADDING = 6.5;
export const LARGE_INPUT_HORIZONTAL_PADDING = 16;
export const NORMAL_INPUT_HORIZONTAL_PADDING = 16;
export const INPUT_SHOW_ERROR_MARGIN = 5;

export const LARGE_INPUT_ICON_OFFSET = 14;
export const NORMAL_INPUT_ICON_OFFSET = 6;

export type IconPosition = "left" | "right";

interface IconProps {
  position?: IconPosition;
  clickable?: boolean;
}

export const Addon = styled.div`
  position: absolute;
  right: -1px;
  top: 1px;
  width: 48px;
  height: calc(100% - 2px);
  border-left: solid 1px ${colors.lighterGrey};
  background-color: ${colors.lightGrey};
  pointer-events: none;
  border-top-right-radius: 5%;
  border-bottom-right-radius: 5%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${colors.grey};
`;

export const Icon = styled.img<IconProps>`
  position: absolute;
  ${(p) => css`
    ${p.position === "right" ? "right:0" : "left:0"};
    ${p.position === "left" && "margin-left: 16px"};
    ${p.position === "right" && "margin-right: 16px"};
  `}
  /* width: ${(p) => (p.position === "left" ? "24px;" : "18px;")};
  height: ${(p) => (p.position === "left" ? "24px;" : "18px;")}; */
  width:24px;
  height: 24px;
  top: 50%;
  ${(p) => p.position === "left" && `padding-right: 8px;`}
  transform: translateY(-50%);
  ${(p) =>
    p.clickable &&
    css`
      pointer-events: initial;
      cursor: pointer;
    `}
`;

export const StyledInput = styled.input<StyledInputProps>`
  ::-webkit-credentials-auto-fill-button {
    position: absolute;
    right: 50px;
  }
  margin-top: 0px;
  margin-bottom: 0px;
  width: calc(100% - 16px);
  ${(p) => p.isSearch && `width: calc(100% - 48px);`}
  cursor: pointer;
  caret-color: ${colors.black};
  color: ${colors.black};
  font-size: 14px;
  line-height: 1.43;
  letter-spacing: normal;
  padding: 13px 0 13px 16px;
  border-radius: 4px;

  ${(p) => p.isSearch && `padding: 9px 0 9px 48px;`}

  border: 1px solid ${(p) => (p.isError ? colors.red : colors.lighterGrey)};
  &::placeholder {
    color: ${colors.grey};
  }
  :hover:enabled {
    border: 1px solid ${(p) => (p.isError ? colors.red : colors.veryLightGrey)};
  }
  &:focus,
  &:active {
    outline: none;
    border: 1px solid ${(p) => (p.isError ? colors.red : colors.blue)};
    :hover {
      border: 1px solid ${(p) => (p.isError ? colors.red : colors.blue)};
    }
  }

  &:disabled {
    color: ${colors.veryLightGrey};
    cursor: not-allowed;
    background-color: ${colors.middleGrey};
  }
`;

export const InputWrapper = styled.div`
  position: relative;
`;

export const InputComponentWrapper = styled.div<WrapperProps>`
  margin-bottom: ${(p) => (p.isError ? `16px; ` : `16px;`)};
  ${(p) => p.isSearch && `margin-bottom: 0px;`}
  width: ${NORMAL_INPUT_WIDTH}px;
  ${(p) => css`
    width: ${p.sizeType === "small" ? SMALL_INPUT_WIDTH : NORMAL_INPUT_WIDTH}px;
  `}
  ${(p) =>
    (p.sizeType === "unset" || p.sizeType === "medium") && `width: 100%;`}
`;
