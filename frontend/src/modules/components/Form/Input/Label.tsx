import colors from "@src/config/styles/colors";
import styled from "styled-components";

const Label = styled.label`
  font-size: 12px;
  font-weight: 600;
  line-height: 1.33;
  color: ${colors.dimmedGrey};
`;
export default Label;
