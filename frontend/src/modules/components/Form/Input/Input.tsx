import {
  Addon,
  Icon,
  IconPosition,
  InputComponentWrapper,
  InputSize,
  InputWrapper,
  StyledInput,
} from "@src/modules/components/Form/Input/Input.components";
import ErrorMessage from "@src/modules/components/Form/shared/ErrorMessage";
import React, {
  ComponentPropsWithoutRef,
  forwardRef,
  FunctionComponent,
  Ref,
} from "react";

export interface InputProps extends ComponentPropsWithoutRef<"input"> {
  error?: string;
  type?: string;
  isDisabled?: boolean;
  isSearch?: boolean;
  icon?: string;
  secondIcon?: string;
  addon?: string;
  iconPosition?: IconPosition;
  onFocus?: () => void;
  onIconClick?: () => void;
  onSecondIconClick?: () => void;
  sizeType?: InputSize;
  showError?: boolean;
}

export const Input: FunctionComponent<
  InputProps & {
    forwardedRef?: Ref<HTMLInputElement>;
  }
> = ({
  error,
  type = "text",
  forwardedRef,
  isDisabled,
  isSearch,
  icon,
  secondIcon,
  iconPosition,
  onIconClick,
  onSecondIconClick,
  onFocus,
  addon,
  sizeType,
  showError = true,
  ...inputProps
}) => (
  <InputComponentWrapper
    isError={!!error}
    sizeType={sizeType}
    isSearch={isSearch}
  >
    <InputWrapper>
      <StyledInput
        {...inputProps}
        ref={forwardedRef}
        isError={!!error}
        isIcon={!!icon}
        isSearch={isSearch}
        iconPosition={iconPosition}
        type={type}
        disabled={isDisabled}
        onFocus={onFocus}
      />
      {addon && <Addon>{addon}</Addon>}
      {icon && (
        <Icon
          src={icon}
          position={iconPosition}
          clickable={!!onIconClick && !isDisabled}
          onClick={onIconClick}
        />
      )}
      {secondIcon && (
        <Icon
          src={secondIcon}
          position={"right"}
          clickable={!!onSecondIconClick && !isDisabled}
          onClick={onSecondIconClick}
        />
      )}
    </InputWrapper>
    {showError && error && <ErrorMessage>{error}</ErrorMessage>}
  </InputComponentWrapper>
);

export default forwardRef<HTMLInputElement, InputProps>((props, ref) => (
  <Input {...props} forwardedRef={ref} />
));
