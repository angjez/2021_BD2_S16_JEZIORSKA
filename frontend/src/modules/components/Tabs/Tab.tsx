import { OptionType } from "@src/modules/components/Tabs/Tabs";
import { Option } from "@src/modules/components/Tabs/Tabs.components";
import React, { FunctionComponent, useCallback } from "react";

interface Props extends OptionType {
  active: boolean;
  onClick: (value: string) => void;
}

const Tab: FunctionComponent<Props> = ({ label, active, onClick, value }) => {
  const onClickHandler = useCallback(() => {
    onClick(value);
  }, [onClick, value]);

  return (
    <Option isActive={active} onClick={onClickHandler}>
      {label}
    </Option>
  );
};

export default Tab;
