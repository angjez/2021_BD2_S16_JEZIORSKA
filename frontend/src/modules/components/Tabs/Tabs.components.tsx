import colors from "@src/config/styles/colors";
import styled from "styled-components";

export const Wrapper = styled.div`
  min-height: 64px;
  background-color: ${colors.blue};
  padding-left: 50px;
  display: flex;
`;

export const Option = styled.div<{ isActive: boolean }>`
  margin-top: auto;
  display: flex;
  height: 100%;
  align-items: center;
  padding: 0 25px;
  font-size: 16px;
  line-height: 1.38;
  color: #c8c8ca;
  cursor: pointer;
  padding-bottom: 26px;
  ${(props) =>
    props.isActive === true &&
    `color: black; font-weight: bold; border-bottom: 4px solid black; padding-bottom: 22px;`}
`;
