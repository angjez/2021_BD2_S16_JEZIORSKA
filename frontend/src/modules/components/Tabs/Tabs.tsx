import Tab from "@src/modules/components/Tabs/Tab";
import { Wrapper } from "@src/modules/components/Tabs/Tabs.components";
import React, { FunctionComponent } from "react";

export interface OptionType {
  label: string;
  value: string;
}

interface Props {
  options: OptionType[];
  currentValue: OptionType["value"];
  onChange: (value: OptionType["value"]) => void;
}

const Tabs: FunctionComponent<Props> = ({
  onChange,
  options,
  currentValue,
}) => (
  <Wrapper>
    {options.map((option) => (
      <Tab
        key={option.label}
        active={option.value === currentValue}
        onClick={onChange}
        label={option.label}
        value={option.value}
      />
    ))}
  </Wrapper>
);

export default Tabs;
