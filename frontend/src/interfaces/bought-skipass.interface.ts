import { SkipassEntryType } from "@src/enums/skipass-entry-type.enum";

export interface BoughtSkipass {
  id: number;
  entryType: SkipassEntryType;
  startsAt?: Date;
  endsAt?: Date;
  entries?: number;
  wasReturned: boolean;
  skierId: number;
  skipassId: number;
}
