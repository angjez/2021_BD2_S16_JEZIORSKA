export interface Skier {
  id: number;
  firstName: string;
  lastName: string;
  dob: Date;
}
