import { SkipassEntryType } from "@src/enums/skipass-entry-type.enum";
import { SkipassType } from "@src/enums/skipass-type.enum";

interface Skipass {
  id: number;
  name: string;
  type: SkipassType;
  entryType: SkipassEntryType;
  durationHours?: number;
  entries?: number;
  price: number;
  startsAt?: Date;
  endsAt?: Date;
}

export default Skipass;
