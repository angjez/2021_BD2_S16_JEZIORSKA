import { SkipassEntryType } from "@src/enums/skipass-entry-type.enum";

export interface NewBoughtSkipass {
  entryType: SkipassEntryType;
  startsAt?: Date;
  endsAt?: Date;
  entries?: number;
  skierId: number;
  skipassId: number;
}
