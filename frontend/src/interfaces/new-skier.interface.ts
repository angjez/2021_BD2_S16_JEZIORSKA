export interface NewSkier {
  firstName: string;
  lastName: string;
  dob: Date;
}
