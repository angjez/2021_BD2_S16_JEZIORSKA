export interface Entry {
    boughtSkipassId: number;
    liftId: number;
    createdAt: Date;
  }