export enum SkipassEntryType {
  TIME = "TIME",
  ENTRIES = "ENTRIES",
}

export enum SkipassType {
  JUNIOR = "JUNIOR",
  STANDARD = "STANDARD",
  SENIOR = "SENIOR",
}
export interface Skipass {
  id: number;
  name: string;
  type: SkipassType;
  entryType: SkipassEntryType;
  durationHours?: number;
  entries?: number;
  price: number;
  startsAt?: Date;
  endsAt?: Date;
}

export interface UpdateSkipass {
  name: string;
  price: number;
  startsAt?: Date;
  endsAt?: Date;
}
