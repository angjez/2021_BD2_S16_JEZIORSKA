export interface Lift {
  id: number;
  name: string;
  isOperational: boolean;
  entriesCount: number;
}
