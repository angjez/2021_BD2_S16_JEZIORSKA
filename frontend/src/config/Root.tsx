import App from "@src/modules/App/App";
import { FunctionComponent } from "react";
import { BrowserRouter } from "react-router-dom";

const Root: FunctionComponent = () => (
  <BrowserRouter>
    <App />
  </BrowserRouter>
);

export default Root;
