const pathNames = {
  cashier: "/cashier",
  skier: "/skier",
  owner: "/owner",
  manager: "/manager",
};

export default pathNames;
