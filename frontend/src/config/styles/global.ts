import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
@import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;400;500;600;700&display=swap');  
@import url("https://fonts.googleapis.com/css?family=PT+Sans:400,700&display=swap");

  body {
    font-family: "Montserrat", "sans-serif";
		margin: 0;
r
    input {
      font-family: inherit;
    }
  }
`;

export default GlobalStyle;
