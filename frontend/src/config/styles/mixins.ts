import colors from "@src/config/styles/colors";
import { css } from "styled-components";

export const defaultGreyBorder = () => css`
  border: solid 1px ${colors.lighterGrey};
`;

export const defaultBoxShadow = () => css`
  box-shadow: 0 2px 4px 0 rgba(33, 37, 41, 0.08);
`;

export const defaultElementBorderRadius = () => css`
  border-radius: 4px;
`;
