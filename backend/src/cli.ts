import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DatabaseModule } from './modules/database/database.module';
import { Seed } from './modules/database/seed';

(async () => {
  const app = await NestFactory.createApplicationContext(AppModule, {
    logger: true, // no logger
  });
  app.select(DatabaseModule).get(Seed).execute();
})();
