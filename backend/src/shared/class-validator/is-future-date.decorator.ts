import { ValidationOptions, registerDecorator, ValidationArguments } from "class-validator";

export function IsFutureDate(validationOptions?: ValidationOptions) {
  return (object: {}, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: {
        message: (validationOptions && validationOptions.message) || "Must be a future date.",
        ...validationOptions,
      },
      validator: {
        validate(value: any, args: ValidationArguments) {
          const now = new Date();
          const comparedDate = new Date(value);
          return comparedDate > now;
        },
      },
    });
  };
}
