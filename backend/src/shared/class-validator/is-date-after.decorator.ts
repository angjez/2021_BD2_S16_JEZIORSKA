import { ValidationOptions, registerDecorator, ValidationArguments } from "class-validator";

export function IsDateAfter(beforeDatePropertyName: string, validationOptions?: ValidationOptions) {
  return (object: {}, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      constraints: [beforeDatePropertyName],
      options: {
        message: (validationOptions && validationOptions.message) || "Provided date must be greater than compared date.",
        ...validationOptions,
      },
      validator: {
        validate(afterDate: Date, args: ValidationArguments) {
          const [beforeDatePropertyName] = args.constraints;
          const beforeDate = new Date((args.object as any)[beforeDatePropertyName]);

          return afterDate > beforeDate;
        },
      },
    });
  };
}
