import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { CreateEntryDto } from './create-entry/create-entry.dto';
import { CreateEntryService } from './create-entry/create-entry.service';
import { EntryService } from './entry.service';
import { EntryDto } from './types/entry.dto';

@ApiTags('entry')
@Controller('entry')
export class EntryController {
  constructor(
    private readonly createEntryService: CreateEntryService,
    private readonly entryService: EntryService,
  ) {}

  @Post()
  @ApiOperation({ description: 'creates new entry' })
  @ApiCreatedResponse()
  @ApiBadRequestResponse()
  @ApiBody({ type: CreateEntryDto })
  async createEntry(@Body() body: CreateEntryDto) {
    return this.createEntryService.createEntry(body);
  }

  @Get()
  @ApiOperation({ description: 'gets entries' })
  @ApiOkResponse({ type: EntryDto })
  async getEntries() {
    return this.entryService.getEntries();
  }

  @Get(':liftId')
  @ApiOperation({ description: 'gets entries with specific lift ID' })
  @ApiOkResponse({ type: EntryDto })
  @ApiParam({ name: 'liftId', type: Number })
  async getEntriesByLiftId(@Param('liftId', new ParseIntPipe()) liftId: number) {
    return this.entryService.getEntriesByLiftId(liftId);
  }

  @Get('/numberOfDays/:liftId:numberOfDays')
  @ApiOperation({ description: 'gets entries in the last days with specific lift ID' })
  @ApiOkResponse({ type: EntryDto })
  @ApiParam({ name: 'liftId', type: Number })
  @ApiParam({ name: 'numberOfDays', type: Number })
  async getEntriesByNumberOfDays(@Param('liftId', new ParseIntPipe()) liftId: number, @Param('numberOfDays', new ParseIntPipe()) numberOfDays: number) {
    return this.entryService.getEntriesByNumberOfDays(liftId, numberOfDays);
  }
}
