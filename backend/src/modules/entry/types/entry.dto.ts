import { ApiProperty, } from '@nestjs/swagger';
import { IsNumber,} from 'class-validator';

export class EntryDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  @IsNumber()
  liftId: number;

  @ApiProperty()
  @IsNumber()
  boughtSkipassId: number;
}
