import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EntryEntity } from '../database/entities/entry.entity';
import { EntryDto } from './types/entry.dto';
import { subDays } from 'date-fns';

@Injectable()
export class EntryService {
  constructor(@InjectRepository(EntryEntity) public readonly repository: Repository<EntryEntity>) {}

  async getEntries(): Promise<EntryDto[]> {
    const entities = await this.repository.find({});
    return entities.map((el) => ({
      id: el.id,
      createdAt: el.createdAt,
      liftId: el.liftId,
      boughtSkipassId: el.boughtSkipassId,
    }));
  }

  async getEntriesByLiftId(id: number): Promise<number> {
    const entitiesNumber = await this.repository
      .createQueryBuilder("entryEntity")
      .select("COUNT(entryEntity.id)", "count")
      .where("entryEntity.liftId = :id", {id})
      .getRawOne();
    return entitiesNumber.count;
  }
  
  async getEntriesByNumberOfDays(id: number, numberOfDays: number): Promise<number> {
    const currentDate = new Date();
    const date = subDays(currentDate, numberOfDays);
    const entities = await this.repository
      .createQueryBuilder("entryEntity")
      .select("COUNT(entryEntity.id)", "count")
      .where("entryEntity.createdAt >= :date", {date})
      .andWhere("entryEntity.liftId = :id", {id})
      .getRawOne();
    return entities.count;
  }
}
