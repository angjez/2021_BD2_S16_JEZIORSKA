import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EntryController } from './entry.controller';
import { EntryService } from './entry.service';
import { EntryEntity } from '../database/entities/entry.entity';
import { CreateEntryService } from './create-entry/create-entry.service';
import { BoughtSkipassModule } from '../bought-skipass/bought-skipass.module';
@Module({
  imports: [TypeOrmModule.forFeature([EntryEntity]), BoughtSkipassModule],
  controllers: [EntryController],
  providers: [EntryService, CreateEntryService],
  exports: [EntryService],
})
export class EntryModule {}
