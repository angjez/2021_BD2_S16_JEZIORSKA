import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsNotEmpty, IsNumber } from 'class-validator';


export class CreateEntryDto {
    @ApiProperty()
    @IsDate()
    @IsNotEmpty()
    createdAt: Date;
  
    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    liftId: number;

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    boughtSkipassId: number;
  }
  