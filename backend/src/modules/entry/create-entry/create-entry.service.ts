import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { EntryService } from '../entry.service';
import { CreateEntryDto } from './create-entry.dto';
import { UpdateBoughtSkipassService } from '../../bought-skipass/update-bought-skipass/update-bought-skipass.service';
import { BoughtSkipassService } from '../../bought-skipass/bought-skipass.service';
@Injectable()
export class CreateEntryService {
  constructor(private readonly boughtBoughtSkipassService: BoughtSkipassService, 
    private readonly updateBoughtSkipassService: UpdateBoughtSkipassService, private readonly entryService: EntryService) {}

  async createEntry(dto: CreateEntryDto) {
    const entry = this.entryService.repository.create({ ...dto });
    const boughtSkipass = await this.boughtBoughtSkipassService.getBoughtSkipass(dto.boughtSkipassId);
    if (!boughtSkipass) throw new NotFoundException();
    if(boughtSkipass.entryType == 'TIME' && (new Date() < boughtSkipass.startsAt || boughtSkipass.endsAt < new Date()))
      throw new UnauthorizedException();
    if(boughtSkipass.entryType == 'ENTRIES') {
      if(boughtSkipass.entries <= 0)
        throw new UnauthorizedException();
      else 
        this.updateBoughtSkipassService.decrementEntriesBoughtSkipass(dto.boughtSkipassId);
    }
    await this.entryService.repository.save(entry);
  }
}
