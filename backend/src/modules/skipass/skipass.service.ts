import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LessThan, MoreThan, Repository } from 'typeorm';
import { SkipassEntity } from '../database/entities/skipass.entity';
import { SkipassDto } from './types/skipass.dto';

@Injectable()
export class SkipassService {
  constructor(@InjectRepository(SkipassEntity) public readonly repository: Repository<SkipassEntity>) {}

  async getSkipass(id: number): Promise<SkipassEntity> {
    return this.repository.findOne({ id, isDeleted: false });
  }

  async getCurrentSkipasses(): Promise<SkipassDto[]> {
    const now = new Date();
    const entities = await this.repository.find({ startsAt: LessThan(now), endsAt: MoreThan(now), isDeleted: false });
    return entities.map((el) => ({
      id: el.id,
      name: el.name,
      type: el.type,
      entryType: el.entryType,
      price: el.price,
      durationHours: el.durationHours,
      entries: el.entries,
    }));
  }

  async getSkipasses(): Promise<SkipassDto[]> {
    const entities = await this.repository.find({ isDeleted: false });
    return entities.map((el) => ({
      id: el.id,
      name: el.name,
      type: el.type,
      entryType: el.entryType,
      price: el.price,
      durationHours: el.durationHours,
      entries: el.entries,
      startsAt: el.startsAt,
      endsAt: el.endsAt,
    }));
  }
}
