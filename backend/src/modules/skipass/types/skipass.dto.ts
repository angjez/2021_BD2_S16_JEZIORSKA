import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { SkipassEntryType } from 'src/modules/skipass/enums/skipass-entry-type.enum';
import { SkipassType } from 'src/modules/skipass/enums/skipass-type.enum';

export class SkipassDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  type: SkipassType;

  @ApiProperty()
  entryType: SkipassEntryType;

  @ApiPropertyOptional()
  durationHours?: number;

  @ApiPropertyOptional()
  entries?: number;

  @ApiProperty()
  price: number;

  @ApiPropertyOptional()
  startsAt?: Date;

  @ApiPropertyOptional()
  endsAt?: Date;
}
