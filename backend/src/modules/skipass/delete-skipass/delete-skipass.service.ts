import { Injectable, NotFoundException } from '@nestjs/common';
import { SkipassService } from '../skipass.service';

@Injectable()
export class DeleteSkipassService {
  constructor(private readonly skipassService: SkipassService) {}

  async deleteSkipass(skipassId: number) {
    const skipass = await this.skipassService.getSkipass(skipassId);
    if (!skipass) throw new NotFoundException();
    skipass.isDeleted = true;
    await this.skipassService.repository.save(skipass);
  }
}
