import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsEnum, IsNotEmpty, IsNumber, IsString, ValidateIf } from 'class-validator';
import { IsDateAfter } from 'src/shared/class-validator/is-date-after.decorator';
import { SkipassEntryType } from '../enums/skipass-entry-type.enum';
import { SkipassType } from '../enums/skipass-type.enum';

export class CreateSkipassDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ enum: SkipassType })
  @IsEnum(SkipassType)
  type: SkipassType;

  @ApiProperty({ enum: SkipassEntryType })
  @IsEnum(SkipassEntryType)
  entryType: SkipassEntryType;

  @ApiProperty({ nullable: true })
  @IsNumber()
  @ValidateIf((o) => o.entryType === SkipassEntryType.TIME)
  durationHours?: number;

  @ApiProperty({ nullable: true })
  @IsNumber()
  @ValidateIf((o) => o.entryType === SkipassEntryType.ENTRIES)
  entries?: number;

  @ApiProperty()
  @IsNumber()
  price: number;

  @ApiProperty()
  @IsDate()
  startsAt: Date;

  @ApiProperty()
  @IsDate()
  @IsDateAfter('startsAt')
  endsAt: Date;
}
