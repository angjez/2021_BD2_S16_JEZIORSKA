import { Injectable } from '@nestjs/common';
import { SkipassService } from '../skipass.service';
import { CreateSkipassDto } from './create-skipass.dto';

@Injectable()
export class CreateSkipassService {
  constructor(private readonly skipassService: SkipassService) {}

  async createSkipass(dto: CreateSkipassDto) {
    const skipass = this.skipassService.repository.create({ ...dto });
    await this.skipassService.repository.save(skipass);
  }
}
