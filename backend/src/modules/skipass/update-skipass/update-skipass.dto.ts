import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { IsDateAfter } from 'src/shared/class-validator/is-date-after.decorator';

export class UpdateSkipassDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsNumber()
  price: number;

  @ApiProperty()
  @IsDate()
  startsAt: Date;

  @ApiProperty()
  @IsDate()
  @IsDateAfter('startsAt')
  endsAt: Date;
}
