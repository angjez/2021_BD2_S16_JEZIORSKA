import { Injectable, NotFoundException } from '@nestjs/common';
import { SkipassService } from '../skipass.service';
import { UpdateSkipassDto } from './update-skipass.dto';

@Injectable()
export class UpdateSkipassService {
  constructor(private readonly skipassService: SkipassService) {}

  async updateSkipass(skipassId: number, dto: UpdateSkipassDto) {
    const skipass = await this.skipassService.getSkipass(skipassId);
    if (!skipass) throw new NotFoundException();
    await this.skipassService.repository.save({ ...skipass, ...dto });
  }
}
