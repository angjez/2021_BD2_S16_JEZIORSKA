import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SkipassEntity } from '../database/entities/skipass.entity';
import { CreateSkipassService } from './create-skipass/create-skipass.service';
import { DeleteSkipassService } from './delete-skipass/delete-skipass.service';
import { SkipassController } from './skipass.controller';
import { SkipassService } from './skipass.service';
import { UpdateSkipassService } from './update-skipass/update-skipass.service';

@Module({
  imports: [TypeOrmModule.forFeature([SkipassEntity])],
  controllers: [SkipassController],
  providers: [SkipassService, CreateSkipassService, DeleteSkipassService, UpdateSkipassService],
  exports: [SkipassService],
})
export class SkipassModule {}
