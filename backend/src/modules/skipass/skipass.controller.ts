import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { CreateSkipassDto } from './create-skipass/create-skipass.dto';
import { CreateSkipassService } from './create-skipass/create-skipass.service';
import { DeleteSkipassService } from './delete-skipass/delete-skipass.service';
import { SkipassService } from './skipass.service';
import { SkipassDto } from './types/skipass.dto';
import { UpdateSkipassDto } from './update-skipass/update-skipass.dto';
import { UpdateSkipassService } from './update-skipass/update-skipass.service';

@ApiTags('skipass')
@Controller('skipass')
export class SkipassController {
  constructor(
    private readonly createSkipassService: CreateSkipassService,
    private readonly skipassService: SkipassService,
    private readonly deleteSkipassService: DeleteSkipassService,
    private readonly updateSkipassService: UpdateSkipassService,
  ) {}

  @Post()
  @ApiOperation({ description: 'creates a new skipass' })
  @ApiCreatedResponse()
  @ApiBadRequestResponse()
  @ApiBody({ type: CreateSkipassDto })
  async createSkipass(@Body() body: CreateSkipassDto) {
    return this.createSkipassService.createSkipass(body);
  }

  @Get('current')
  @ApiOperation({ description: 'gets current skipasses' })
  @ApiOkResponse({ type: SkipassDto })
  async getCurrentSkipasses() {
    return this.skipassService.getCurrentSkipasses();
  }

  @Get()
  @ApiOperation({ description: 'gets skipasses' })
  @ApiOkResponse({ type: SkipassDto })
  async getSkipasses() {
    return this.skipassService.getSkipasses();
  }

  @Delete(':id')
  @ApiOperation({ description: 'deletes a skipass' })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  @ApiParam({ name: 'id', type: Number })
  async deleteSkipass(@Param('id', new ParseIntPipe()) id: number) {
    return this.deleteSkipassService.deleteSkipass(id);
  }

  @Put(':id')
  @ApiOperation({ description: 'updates a skipass' })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  @ApiParam({ name: 'id', type: Number })
  @ApiBody({ type: UpdateSkipassDto })
  async updatekipas(@Param('id', new ParseIntPipe()) id: number, @Body() body: UpdateSkipassDto) {
    return this.updateSkipassService.updateSkipass(id, body);
  }
}
