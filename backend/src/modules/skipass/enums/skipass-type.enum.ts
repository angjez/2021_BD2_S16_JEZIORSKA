export enum SkipassType {
  JUNIOR = 'JUNIOR',
  STANDARD = 'STANDARD',
  SENIOR = 'SENIOR',
}
