import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommandModule } from 'nestjs-command';
import connectionOptions from '../../../ormconfig';
import { BoughtSkipassModule } from '../bought-skipass/bought-skipass.module';
import { LiftModule } from '../lift/lift.module';
import { SkierModule } from '../skier/skier.module';
import { SkipassModule } from '../skipass/skipass.module';
import { Seed } from './seed';

@Module({
  imports: [TypeOrmModule.forRoot(connectionOptions), CommandModule, SkipassModule, LiftModule, SkierModule, BoughtSkipassModule],
  providers: [Seed],
})
export class DatabaseModule {}
