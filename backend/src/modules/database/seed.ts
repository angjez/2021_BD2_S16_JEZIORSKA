import { Inject, Injectable } from '@nestjs/common';
import { addHours, addMonths } from 'date-fns';
import { Command } from 'nestjs-command';
import { BoughtSkipassService } from '../bought-skipass/bought-skipass.service';
import { LiftService } from '../lift/lift.service';
import { SkierService } from '../skier/skier.service';
import { SkipassEntryType } from '../skipass/enums/skipass-entry-type.enum';
import { SkipassType } from '../skipass/enums/skipass-type.enum';
import { SkipassService } from '../skipass/skipass.service';

@Injectable()
export class Seed {
  constructor(
    @Inject(SkierService) private readonly skierService: SkierService,
    @Inject(SkipassService) private readonly skipassService: SkipassService,
    @Inject(LiftService) private readonly liftService: LiftService,
    @Inject(BoughtSkipassService) private readonly boughtSkipassService: BoughtSkipassService,
  ) {}

  @Command({
    command: 'seed',
    describe: 'Seed',
    autoExit: false,
  })
  async execute() {
    await this.truncate();
    await this.seed();
  }

  private async seed() {
    await this.seedSkiers();
    await this.seedSkipasses();
    await this.seedLifts();
    await this.seedBoughtSkipasses();
  }

  private async truncate() {
    await this.skipassService.repository.query('TRUNCATE TABLE skipass CASCADE');
    await this.skipassService.repository.query('TRUNCATE TABLE lifts CASCADE');
    await this.skipassService.repository.query('TRUNCATE TABLE skiers CASCADE');
    await this.skipassService.repository.query('TRUNCATE TABLE bought_skipasses CASCADE');
  }

  private async seedSkiers() {
    await this.skierService.repository.insert([
      { id: 1, firstName: 'Jan', lastName: 'Kowalski', dob: new Date(1990, 2, 10) },
      { id: 2, firstName: 'Sebastian', lastName: 'Marek', dob: new Date(1960, 4, 8) },
      { id: 3, firstName: 'Karol', lastName: 'Nowak', dob: new Date(1992, 8, 23) },
      { id: 4, firstName: 'Anna', lastName: 'Piotrowska', dob: new Date(1982, 9, 3) },
      { id: 5, firstName: 'Magdalena', lastName: 'Kowalska', dob: new Date(1997, 10, 29) },
    ]);
  }

  private async seedSkipasses() {
    const firstSeasonStart = new Date();
    const firstSeasonEnd = addMonths(firstSeasonStart, 4);
    const secondSeasonEnd = addMonths(firstSeasonStart, 8);
    await this.skipassService.repository.insert([
      {
        id: 1,
        name: 'Junior 5 entries',
        type: SkipassType.JUNIOR,
        entryType: SkipassEntryType.ENTRIES,
        durationHours: null,
        entries: 5,
        price: 15,
        startsAt: firstSeasonStart,
        endsAt: firstSeasonEnd,
        isDeleted: false,
      },
      {
        id: 2,
        name: 'Junior 10 entries',
        type: SkipassType.JUNIOR,
        entryType: SkipassEntryType.ENTRIES,
        durationHours: null,
        entries: 10,
        price: 30,
        startsAt: firstSeasonStart,
        endsAt: firstSeasonEnd,
        isDeleted: false,
      },
      {
        id: 3,
        name: 'Standard 5 entries',
        type: SkipassType.STANDARD,
        entryType: SkipassEntryType.ENTRIES,
        durationHours: null,
        entries: 5,
        price: 30,
        startsAt: firstSeasonStart,
        endsAt: firstSeasonEnd,
        isDeleted: false,
      },
      {
        id: 4,
        name: 'Standard 10 entries',
        type: SkipassType.STANDARD,
        entryType: SkipassEntryType.ENTRIES,
        durationHours: null,
        entries: 10,
        price: 60,
        startsAt: firstSeasonStart,
        endsAt: firstSeasonEnd,
        isDeleted: false,
      },
      {
        id: 5,
        name: 'Senior 5 entries',
        type: SkipassType.SENIOR,
        entryType: SkipassEntryType.ENTRIES,
        durationHours: null,
        entries: 5,
        price: 20,
        startsAt: firstSeasonStart,
        endsAt: firstSeasonEnd,
        isDeleted: false,
      },
      {
        id: 6,
        name: 'Senior 10 entries',
        type: SkipassType.SENIOR,
        entryType: SkipassEntryType.ENTRIES,
        durationHours: null,
        entries: 10,
        price: 40,
        startsAt: firstSeasonStart,
        endsAt: firstSeasonEnd,
        isDeleted: false,
      },
      {
        id: 7,
        name: 'Junior single day',
        type: SkipassType.JUNIOR,
        entryType: SkipassEntryType.TIME,
        durationHours: 8,
        entries: null,
        price: 50,
        startsAt: firstSeasonStart,
        endsAt: firstSeasonEnd,
        isDeleted: false,
      },
      {
        id: 8,
        name: 'Standard single day',
        type: SkipassType.STANDARD,
        entryType: SkipassEntryType.TIME,
        durationHours: 8,
        entries: null,
        price: 80,
        startsAt: firstSeasonStart,
        endsAt: firstSeasonEnd,
        isDeleted: false,
      },
      {
        id: 9,
        name: 'Senior single day',
        type: SkipassType.SENIOR,
        entryType: SkipassEntryType.TIME,
        durationHours: 8,
        entries: null,
        price: 70,
        startsAt: firstSeasonStart,
        endsAt: firstSeasonEnd,
        isDeleted: false,
      },
      {
        id: 10,
        name: 'Junior single day second season',
        type: SkipassType.JUNIOR,
        entryType: SkipassEntryType.TIME,
        durationHours: 8,
        entries: null,
        price: 35,
        startsAt: firstSeasonEnd,
        endsAt: secondSeasonEnd,
        isDeleted: false,
      },
      {
        id: 11,
        name: 'Standard single day second season',
        type: SkipassType.STANDARD,
        entryType: SkipassEntryType.TIME,
        durationHours: 8,
        entries: null,
        price: 60,
        startsAt: firstSeasonEnd,
        endsAt: secondSeasonEnd,
        isDeleted: false,
      },
      {
        id: 12,
        name: 'Senior single day second season',
        type: SkipassType.SENIOR,
        entryType: SkipassEntryType.TIME,
        durationHours: 8,
        entries: null,
        price: 45,
        startsAt: firstSeasonEnd,
        endsAt: secondSeasonEnd,
        isDeleted: false,
      },
    ]);
  }

  private async seedLifts() {
    await this.liftService.repository.insert([
      {
        id: 1,
        name: 'Cibes',
        isOperational: true,
      },
      {
        id: 2,
        name: 'Kone',
        isOperational: true,
      },
      {
        id: 3,
        name: 'Telluride',
        isOperational: true,
      },
      {
        id: 4,
        name: 'Aspen',
        isOperational: false,
      },
      {
        id: 5,
        name: 'Cortina',
        isOperational: false,
      },
    ]);
  }

  private async seedBoughtSkipasses() {
    const now = new Date();
    await this.boughtSkipassService.repository.insert([
      {
        id: 1,
        entryType: SkipassEntryType.ENTRIES,
        startsAt: null,
        endsAt: null,
        entries: 5,
        wasReturned: false,
        skierId: 1,
        skipassId: 3,
      },
      {
        id: 2,
        entryType: SkipassEntryType.ENTRIES,
        startsAt: null,
        endsAt: null,
        entries: 10,
        wasReturned: false,
        skierId: 2,
        skipassId: 6,
      },
      {
        id: 3,
        entryType: SkipassEntryType.TIME,
        startsAt: now,
        endsAt: addHours(now, 8),
        entries: null,
        wasReturned: false,
        skierId: 3,
        skipassId: 8,
      },
      {
        id: 4,
        entryType: SkipassEntryType.TIME,
        startsAt: now,
        endsAt: addHours(now, 8),
        entries: null,
        wasReturned: false,
        skierId: 4,
        skipassId: 8,
      },
      {
        id: 5,
        entryType: SkipassEntryType.TIME,
        startsAt: now,
        endsAt: addHours(now, 8),
        entries: null,
        wasReturned: false,
        skierId: 5,
        skipassId: 7,
      },
    ]);
  }
}
