import { SkipassEntryType } from 'src/modules/skipass/enums/skipass-entry-type.enum';
import { SkipassType } from 'src/modules/skipass/enums/skipass-type.enum';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { BoughtSkipassEntity } from './bought-skipass.entity';

@Entity('skipass')
export class SkipassEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  name: string;

  @Column('enum', { nullable: false, enum: SkipassType })
  type: SkipassType;

  @Column('enum', { nullable: false, enum: SkipassEntryType })
  entryType: SkipassEntryType;

  @Column('int', { nullable: true })
  durationHours: number | null;

  @Column('int', { nullable: true })
  entries: number | null;

  @Column('money', { nullable: false })
  price: number;

  @Column({ nullable: false })
  startsAt: Date;

  @Column({ nullable: false })
  endsAt: Date;

  @Column({ nullable: false, default: false })
  isDeleted: boolean;

  @OneToMany(() => BoughtSkipassEntity, (boughtSkipass) => boughtSkipass.skipass)
  boughtSkipasses: BoughtSkipassEntity[];
}
