import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BoughtSkipassEntity } from './bought-skipass.entity';
import { LiftEntity } from './lift.entity';

@Entity('entries')
export class EntryEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  createdAt: Date;

  @ManyToOne(() => LiftEntity, (lift) => lift.entries)
  lift: LiftEntity;
  @Column('int', { nullable: false })
  liftId: number;

  @ManyToOne(() => BoughtSkipassEntity, (boughtSkipass) => boughtSkipass.entries)
  boughtSkipasses: BoughtSkipassEntity;
  @Column('int', { nullable: false })
  boughtSkipassId: number;
}
