import { SkipassEntryType } from 'src/modules/skipass/enums/skipass-entry-type.enum';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { SkierEntity } from './skier.entity';
import { SkipassEntity } from './skipass.entity';

@Entity('bought_skipasses')
export class BoughtSkipassEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('enum', { nullable: false, enum: SkipassEntryType })
  entryType: SkipassEntryType;

  @Column({ nullable: true })
  startsAt: Date | null;

  @Column({ nullable: true })
  endsAt: Date | null;

  @Column('int', { nullable: true })
  entries: number | null;

  @Column({ default: false, nullable: false })
  wasReturned: boolean;

  @ManyToOne(() => SkierEntity, (skier) => skier.boughtSkipasses)
  skier: SkierEntity;
  @Column('int', { nullable: false })
  skierId: number;

  @ManyToOne(() => SkipassEntity, (skipass) => skipass.boughtSkipasses)
  skipass: SkipassEntity;
  @Column('int', { nullable: false })
  skipassId: number;
}
