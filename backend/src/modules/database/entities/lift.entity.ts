import { Entity, PrimaryGeneratedColumn, ManyToOne, Column, OneToOne, OneToMany } from 'typeorm';
import { BoughtSkipassEntity } from './bought-skipass.entity';
import { EntryEntity } from './entry.entity';

@Entity('lifts')
export class LiftEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  isOperational: boolean;

  @OneToMany(() => EntryEntity, (entry) => entry.lift)
  entries: EntryEntity[];

  @Column({ nullable: false, default: false })
  isDeleted: boolean;
}
