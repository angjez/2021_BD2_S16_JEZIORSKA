import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { BoughtSkipassEntity } from './bought-skipass.entity';

@Entity('skiers')
export class SkierEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  firstName: string;

  @Column({ nullable: false })
  lastName: string;

  @Column({ nullable: false })
  dob: Date;

  @OneToMany(() => BoughtSkipassEntity, (boughtSkipass) => boughtSkipass.skier)
  boughtSkipasses: BoughtSkipassEntity[];
}
