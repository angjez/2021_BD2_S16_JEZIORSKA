import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { SkierEntity } from '../database/entities/skier.entity';
import { CreateSkierDto } from './create-skier/create-skier.dto';
import { CreateSkierService } from './create-skier/create-skier.service';
import { DeleteSkierService } from './delete-skier/delete-skier.service';
import { SkierService } from './skier.service';

@ApiTags('skier')
@Controller('skier')
export class SkierController {
  constructor(
    private readonly skierService: SkierService,
    private readonly createSkierService: CreateSkierService,
    private readonly deleteSkierService: DeleteSkierService,
  ) {}

  @Post()
  @ApiOperation({ description: 'creates a new skier' })
  @ApiCreatedResponse({ type: SkierEntity })
  @ApiBadRequestResponse()
  @ApiBody({ type: CreateSkierDto })
  async createSkier(@Body() body: CreateSkierDto) {
    return this.createSkierService.createSkier(body);
  }

  @Get(':id')
  @ApiOperation({ description: 'gets skier with id' })
  @ApiOkResponse({ type: SkierEntity })
  @ApiNotFoundResponse()
  @ApiParam({ name: 'id', type: Number })
  async getSkierWithId(@Param('id', new ParseIntPipe()) id: number) {
    return this.skierService.getSkier(id);
  }

  @Get()
  @ApiOperation({ description: 'gets skiers' })
  @ApiOkResponse({ type: SkierEntity })
  async getSkiers() {
    return this.skierService.getSkiers();
  }

  @Delete(':id')
  @ApiOperation({ description: 'deletes a skier' })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  @ApiParam({ name: 'id', type: Number })
  async deleteSkier(@Param('id', new ParseIntPipe()) id: number) {
    return this.deleteSkierService.deleteSkier(id);
  }
}
