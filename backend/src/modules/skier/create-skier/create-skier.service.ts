import { BadRequestException, Injectable, InternalServerErrorException } from '@nestjs/common';
import { SkierEntity } from 'src/modules/database/entities/skier.entity';
import { SkierService } from '../skier.service';
import { CreateSkierDto } from './create-skier.dto';

@Injectable()
export class CreateSkierService {
  constructor(private readonly skierService: SkierService) {}

  async createSkier(dto: CreateSkierDto): Promise<SkierEntity> {
    if (dto.dob > new Date()) throw new BadRequestException();
    const skier = this.skierService.repository.create({ ...dto });
    try {
      const newSkier = await this.skierService.repository.save(skier);
      return newSkier;
    } catch (e) {
      throw new InternalServerErrorException();
    }
  }
}
