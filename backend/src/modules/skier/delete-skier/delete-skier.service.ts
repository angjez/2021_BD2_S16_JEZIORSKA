import { Injectable, NotFoundException } from '@nestjs/common';
import { SkierService } from '../skier.service';

@Injectable()
export class DeleteSkierService {
  constructor(private readonly skierService: SkierService) {}

  async deleteSkier(id: number) {
    const skier = await this.skierService.getSkier(id);
    if (!skier) throw new NotFoundException();
    await this.skierService.repository.remove(skier);
  }
}
