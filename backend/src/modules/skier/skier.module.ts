import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SkierEntity } from '../database/entities/skier.entity';
import { CreateSkierService } from './create-skier/create-skier.service';
import { DeleteSkierService } from './delete-skier/delete-skier.service';
import { SkierController } from './skier.controller';
import { SkierService } from './skier.service';

@Module({
  imports: [TypeOrmModule.forFeature([SkierEntity])],
  controllers: [SkierController],
  providers: [SkierService, CreateSkierService, DeleteSkierService],
  exports: [SkierService],
})
export class SkierModule {}
