import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SkierEntity } from '../database/entities/skier.entity';
import { SkierDto } from './types/skier.dto';

@Injectable()
export class SkierService {
  constructor(@InjectRepository(SkierEntity) public readonly repository: Repository<SkierEntity>) {}

  async getSkier(id: number): Promise<SkierEntity> {
    const skier = await this.repository.findOne({ id });
    if (!skier) throw new NotFoundException();
    return skier;
  }

  async getSkiers(): Promise<SkierEntity[]> {
    return this.repository.find();
  }
}
