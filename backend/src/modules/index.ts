import { BoughtSkipassModule } from './bought-skipass/bought-skipass.module';
import { CommonModule } from './common/common.module';
import { DatabaseModule } from './database/database.module';
import { EntryModule } from './entry/entry.module';
import { LiftModule } from './lift/lift.module';
import { SkipassModule } from './skipass/skipass.module';
import { SkierModule } from './skier/skier.module';
import { ReportModule } from './report/report.module';

export const modules = [CommonModule, DatabaseModule, SkipassModule, BoughtSkipassModule, SkierModule, EntryModule, LiftModule, ReportModule];
