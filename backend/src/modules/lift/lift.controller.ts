import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { CreateLiftDto } from './create-lift/create-lift.dto';
import { CreateLiftService } from './create-lift/create-lift.service';
import { DeleteLiftService } from './delete-lift/delete-lift.service';
import { LiftService } from './lift.service';
import { LiftDto } from './types/lift.dto';
import { UpdateLiftDto } from './update-lift/update-lift.dto';
import { UpdateLiftService } from './update-lift/update-lift.service';

@ApiTags('lift')
@Controller('lift')
export class LiftController {
  constructor(
    private readonly createLiftService: CreateLiftService,
    private readonly liftService: LiftService,
    private readonly deleteLiftService: DeleteLiftService,
    private readonly updateLiftService: UpdateLiftService,
  ) {}

  @Post()
  @ApiOperation({ description: 'creates a new lift' })
  @ApiCreatedResponse()
  @ApiBadRequestResponse()
  @ApiBody({ type: CreateLiftDto })
  async createLift(@Body() body: CreateLiftDto) {
    return this.createLiftService.createLift(body);
  }

  @Get('operational')
  @ApiOperation({ description: 'gets operational lifts' })
  @ApiOkResponse({ type: LiftDto })
  async getCurrentLift() {
    return this.liftService.getOperationalLifts();
  }

  @Get()
  @ApiOperation({ description: 'gets lifts' })
  @ApiOkResponse({ type: LiftDto })
  async getLift() {
    return this.liftService.getLifts();
  }

  @Delete(':id')
  @ApiOperation({ description: 'deletes a lift' })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  @ApiParam({ name: 'id', type: Number })
  async deleteLift(@Param('id', new ParseIntPipe()) id: number) {
    return this.deleteLiftService.deleteLift(id);
  }

  @Put(':id')
  @ApiOperation({ description: 'updates a lift' })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  @ApiParam({ name: 'id', type: Number })
  @ApiBody({ type: LiftDto })
  async updateLift(@Param('id', new ParseIntPipe()) id: number, @Body() body: UpdateLiftDto) {
    return this.updateLiftService.updateLift(id, body);
  }
}
