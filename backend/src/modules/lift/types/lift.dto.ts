import { ApiProperty } from '@nestjs/swagger';

export class LiftDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  isOperational: boolean;
}
