import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";
import {LiftController} from "./lift.controller";
import {LiftService} from "./lift.service";
import {CreateLiftService} from "./create-lift/create-lift.service";
import {DeleteLiftService} from "./delete-lift/delete-lift.service";
import {UpdateLiftService} from "./update-lift/update-lift.service";
import {LiftEntity} from "../database/entities/lift.entity";

@Module({
    imports: [TypeOrmModule.forFeature([LiftEntity])],
    controllers: [LiftController],
    providers: [LiftService, CreateLiftService, DeleteLiftService, UpdateLiftService],
    exports: [LiftService],
})
export class LiftModule {}