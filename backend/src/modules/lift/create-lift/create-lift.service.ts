import { Injectable } from '@nestjs/common';
import { LiftService } from '../lift.service';
import { CreateLiftDto } from './create-lift.dto';

@Injectable()
export class CreateLiftService {
    constructor(private readonly liftService: LiftService) {}

    async createLift(dto: CreateLiftDto) {
        const lift = this.liftService.repository.create({ ...dto });
        await this.liftService.repository.save(lift);
    }
}