import { ApiProperty } from '@nestjs/swagger';
import {IsBoolean, IsNotEmpty, IsString} from 'class-validator';

export class UpdateLiftDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsBoolean()
    isOperational: boolean;
}
