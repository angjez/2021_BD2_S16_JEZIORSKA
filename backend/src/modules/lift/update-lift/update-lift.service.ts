import { Injectable, NotFoundException } from '@nestjs/common';
import { LiftService } from '../lift.service';
import { UpdateLiftDto } from './update-lift.dto';

@Injectable()
export class UpdateLiftService {
    constructor(private readonly liftService: LiftService) {}

    async updateLift(liftId: number, dto: UpdateLiftDto) {
        const lift = await this.liftService.getLift(liftId);
        if (!lift) throw new NotFoundException();
        await this.liftService.repository.save({ ...lift, ...dto });
    }
}
