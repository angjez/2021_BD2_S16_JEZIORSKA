import { Injectable, NotFoundException } from '@nestjs/common';
import {LiftService} from "../lift.service";

@Injectable()
export class DeleteLiftService {
    constructor(private readonly liftService: LiftService) {}

    async deleteLift(liftId: number) {
        const lift = await this.liftService.getLift(liftId);
        if (!lift) throw new NotFoundException();
        lift.isDeleted = true;
        await this.liftService.repository.save(lift);
    }
}
