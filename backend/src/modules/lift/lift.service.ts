import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LiftEntity } from '../database/entities/lift.entity';
import { LiftDto } from './types/lift.dto';

@Injectable()
export class LiftService {
    constructor(@InjectRepository(LiftEntity) public readonly repository: Repository<LiftEntity>) {}

    async getLift(id: number): Promise<LiftEntity> {
        return this.repository.findOne({ id, isDeleted: false });
    }

    async getOperationalLifts(): Promise<LiftDto[]> {
        const entities = await this.repository.find({ isOperational: true , isDeleted: false});
        return entities.map((el) => ({
            id: el.id,
            name: el.name,
            isOperational: el.isOperational,
        }));
    }

    async getLifts(): Promise<LiftDto[]> {
        const entities = await this.repository.find({ isDeleted: false });
        return entities.map((el) => ({
            id: el.id,
            name: el.name,
            isOperational: el.isOperational,
        }));
    }

}
