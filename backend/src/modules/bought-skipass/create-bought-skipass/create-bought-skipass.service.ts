import { BadRequestException, Injectable } from '@nestjs/common';
import { BoughtSkipassService } from '../bought-skipass.service';
import { CreateBoughtSkipassDto } from './create-bought-skipass.dto';

@Injectable()
export class CreateBoughtSkipassService {
  constructor(private readonly boughtSkipassService: BoughtSkipassService) {}

  async createBoughtSkipass(dto: CreateBoughtSkipassDto) {
    if (dto.startsAt < new Date()) throw new BadRequestException();
    if (dto.endsAt < dto.startsAt) throw new BadRequestException();
    if (dto.endsAt < new Date()) throw new BadRequestException();
    const boughtSkipass = this.boughtSkipassService.repository.create({ ...dto });
    await this.boughtSkipassService.repository.save(boughtSkipass);
  }
}
