import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsDate, IsEnum, IsNumber, ValidateIf } from 'class-validator';
import { SkipassEntryType } from 'src/modules/skipass/enums/skipass-entry-type.enum';
import { IsDateAfter } from 'src/shared/class-validator/is-date-after.decorator';

export class CreateBoughtSkipassDto {
  @ApiProperty({ enum: SkipassEntryType })
  @IsEnum(SkipassEntryType)
  entryType: SkipassEntryType;

  @ApiProperty({ nullable: true })
  @IsDate()
  @ValidateIf((o) => o.entryType === SkipassEntryType.TIME)
  startsAt?: Date;

  @ApiProperty({ nullable: true })
  @IsDate()
  @IsDateAfter('startsAt')
  @ValidateIf((o) => o.entryType === SkipassEntryType.TIME)
  endsAt?: Date;

  @ApiProperty({ nullable: true })
  @IsNumber()
  @ValidateIf((o) => o.entryType === SkipassEntryType.ENTRIES)
  entries?: number;

  @ApiProperty()
  @IsNumber()
  skierId: number;

  @ApiProperty()
  @IsNumber()
  skipassId: number;
}
