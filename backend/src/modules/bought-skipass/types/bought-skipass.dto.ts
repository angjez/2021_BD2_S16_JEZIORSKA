import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { SkipassEntryType } from 'src/modules/skipass/enums/skipass-entry-type.enum';

export class BoughtSkipassDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  entryType: SkipassEntryType;

  @ApiPropertyOptional()
  startsAt?: Date;

  @ApiPropertyOptional()
  endsAt?: Date;

  @ApiPropertyOptional()
  entries?: number;

  @ApiProperty()
  wasReturned: boolean;

  @ApiProperty()
  skierId: number;

  @ApiProperty()
  skipassId: number;
}
