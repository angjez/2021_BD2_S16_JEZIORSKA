import { Body, Controller, Get, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { BoughtSkipassService } from './bought-skipass.service';
import { CreateBoughtSkipassDto } from './create-bought-skipass/create-bought-skipass.dto';
import { CreateBoughtSkipassService } from './create-bought-skipass/create-bought-skipass.service';
import { BoughtSkipassDto } from './types/bought-skipass.dto';
import { UpdateBoughtSkipassDto } from './update-bought-skipass/update-bought-skipass.dto';
import { UpdateBoughtSkipassService } from './update-bought-skipass/update-bought-skipass.service';

@ApiTags('bought-skipass')
@Controller('bought-skipass')
export class BoughtSkipassController {
  constructor(
    private readonly createBoughtSkipassService: CreateBoughtSkipassService,
    private readonly updateBoughtSkipassService: UpdateBoughtSkipassService,
    private readonly boughtSkipassService: BoughtSkipassService,
  ) {}

  @Post()
  @ApiOperation({ description: 'creates a new bought skipass' })
  @ApiCreatedResponse()
  @ApiBadRequestResponse()
  @ApiBody({ type: CreateBoughtSkipassDto })
  async createBoughtSkipass(@Body() body: CreateBoughtSkipassDto) {
    return this.createBoughtSkipassService.createBoughtSkipass(body);
  }

  @Get()
  @ApiOperation({ description: 'gets all bought skipasses' })
  @ApiOkResponse({ type: BoughtSkipassDto })
  async getAllBoughtSkipasses() {
    return this.boughtSkipassService.getAllBoughtSkipasses();
  }

  @Get('/unreturned/:skierId')
  @ApiOperation({ description: 'gets all unreturned skiers skipasses' })
  @ApiOkResponse({ type: BoughtSkipassDto })
  @ApiNotFoundResponse()
  @ApiParam({ name: 'skierId', type: Number })
  async getUnreturnedBoughtSkipasses(@Param('skierId', new ParseIntPipe()) skierId: number) {
    return this.boughtSkipassService.getUnreturnedBoughtSkipasses(skierId);
  }

  @Get('/bySkier/:skierId')
  @ApiOperation({ description: 'gets skipasses bought by skier id' })
  @ApiOkResponse({ type: BoughtSkipassDto })
  @ApiNotFoundResponse()
  @ApiParam({ name: 'skierId', type: Number })
  async getBoughtSkipasses(@Param('skierId', new ParseIntPipe()) skierId: number) {
    return this.boughtSkipassService.getBoughtSkipasses(skierId);
  }

  @Get('/unreturned')
  @ApiOperation({ description: 'gets all unreturned skipasses' })
  @ApiOkResponse({ type: BoughtSkipassDto })
  async getAllUnreturnedBoughtSkipasses() {
    return this.boughtSkipassService.getAllUnreturnedBoughtSkipasses();
  }

  @Get(':id')
  @ApiOperation({ description: 'gets skipass by id' })
  @ApiOkResponse({ type: BoughtSkipassDto })
  @ApiNotFoundResponse()
  @ApiParam({ name: 'id', type: Number })
  async getBoughtSkipassesById(@Param('id', new ParseIntPipe()) id: number) {
    return this.boughtSkipassService.getBoughtSkipass(id);
  }

  @Put(':id')
  @ApiOperation({ description: 'updates bought skipass' })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  @ApiParam({ name: 'id', type: Number })
  @ApiBody({ type: UpdateBoughtSkipassDto })
  async updateBoughtSkipass(@Param('id', new ParseIntPipe()) id: number, @Body() body: UpdateBoughtSkipassDto) {
    return this.updateBoughtSkipassService.updateBoughtSkipass(id, body);
  }
  @Put('/entries/:id')
  @ApiOperation({ description: 'decrement entries bought skipass' })
  @ApiOkResponse()
  @ApiNotFoundResponse()
  @ApiParam({ name: 'id', type: Number })
  async decrementEntriesBoughtSkipass(@Param('id', new ParseIntPipe()) id: number) {
    return this.updateBoughtSkipassService.decrementEntriesBoughtSkipass(id);
  }
}
