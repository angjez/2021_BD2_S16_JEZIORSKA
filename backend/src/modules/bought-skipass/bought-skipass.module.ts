import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { BoughtSkipassEntity } from '../database/entities/bought-skipass.entity';
import { BoughtSkipassController } from './bought-skipass.controller';
import { BoughtSkipassService } from './bought-skipass.service';
import { CreateBoughtSkipassService } from './create-bought-skipass/create-bought-skipass.service';
import { UpdateBoughtSkipassService } from './update-bought-skipass/update-bought-skipass.service';
import { SkierEntity } from '../database/entities/skier.entity';

@Module({
  imports: [TypeOrmModule.forFeature([BoughtSkipassEntity]), TypeOrmModule.forFeature([SkierEntity])],
  controllers: [BoughtSkipassController],
  providers: [CreateBoughtSkipassService, UpdateBoughtSkipassService, BoughtSkipassService],
  exports: [BoughtSkipassService, UpdateBoughtSkipassService],
})
export class BoughtSkipassModule {}
