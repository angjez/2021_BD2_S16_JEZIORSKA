import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BoughtSkipassEntity } from '../database/entities/bought-skipass.entity';
import { SkierEntity } from '../database/entities/skier.entity';
import { BoughtSkipassDto } from './types/bought-skipass.dto';

@Injectable()
export class BoughtSkipassService {
  constructor(
    @InjectRepository(BoughtSkipassEntity) public readonly repository: Repository<BoughtSkipassEntity>,
    @InjectRepository(SkierEntity) public readonly skierRepository: Repository<SkierEntity>,
  ) {}

  async getBoughtSkipasses(skierId: number): Promise<BoughtSkipassDto[]> {
    if (!(await this.skierRepository.findOne({ id: skierId }))) throw new NotFoundException();
    const entities = await this.repository.find({ skierId });
    return entities.map((el) => ({
      id: el.id,
      entryType: el.entryType,
      startsAt: el.startsAt,
      endsAt: el.endsAt,
      entries: el.entries,
      wasReturned: el.wasReturned,
      skierId: el.skierId,
      skipassId: el.skipassId,
    }));
  }

  async getUnreturnedBoughtSkipasses(skierId: number): Promise<BoughtSkipassDto[]> {
    if (!(await this.skierRepository.findOne({ id: skierId }))) throw new NotFoundException();
    const entities = await this.repository.find({ skierId, wasReturned: false });
    return entities.map((el) => ({
      id: el.id,
      entryType: el.entryType,
      startsAt: el.startsAt,
      endsAt: el.endsAt,
      entries: el.entries,
      wasReturned: el.wasReturned,
      skierId: el.skierId,
      skipassId: el.skipassId,
    }));
  }

  async getAllBoughtSkipasses(): Promise<BoughtSkipassDto[]> {
    const entities = await this.repository.find({});
    return entities.map((el) => ({
      id: el.id,
      entryType: el.entryType,
      startsAt: el.startsAt,
      endsAt: el.endsAt,
      entries: el.entries,
      wasReturned: el.wasReturned,
      skierId: el.skierId,
      skipassId: el.skipassId,
    }));
  }

  async getAllUnreturnedBoughtSkipasses(): Promise<BoughtSkipassDto[]> {
    const entities = await this.repository.find({ wasReturned: false });
    return entities.map((el) => ({
      id: el.id,
      entryType: el.entryType,
      startsAt: el.startsAt,
      endsAt: el.endsAt,
      entries: el.entries,
      wasReturned: el.wasReturned,
      skierId: el.skierId,
      skipassId: el.skipassId,
    }));
  }

  async getBoughtSkipass(id: number): Promise<BoughtSkipassDto> {
    const entity = await this.repository.findOne({ id });
    if (!entity) throw new NotFoundException();
    return {
      id: entity.id,
      entryType: entity.entryType,
      startsAt: entity.startsAt,
      endsAt: entity.endsAt,
      entries: entity.entries,
      wasReturned: entity.wasReturned,
      skierId: entity.skierId,
      skipassId: entity.skipassId,
    };
  }
}
