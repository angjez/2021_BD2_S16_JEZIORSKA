import { Injectable, NotFoundException } from '@nestjs/common';
import { BoughtSkipassService } from '../bought-skipass.service';
import { UpdateBoughtSkipassDto } from './update-bought-skipass.dto';

@Injectable()
export class UpdateBoughtSkipassService {
  constructor(private readonly boughtSkipassService: BoughtSkipassService) {}

  async updateBoughtSkipass(id: number, dto: UpdateBoughtSkipassDto) {
    const boughtSkipass = await this.boughtSkipassService.getBoughtSkipass(id);
    if (!boughtSkipass) throw new NotFoundException();
    await this.boughtSkipassService.repository.save({ ...boughtSkipass, ...dto });
  }

  async decrementEntriesBoughtSkipass(id: number) {
    const boughtSkipass = await this.boughtSkipassService.getBoughtSkipass(id);
    if (!boughtSkipass) throw new NotFoundException();
    await this.boughtSkipassService.repository.update({ ...boughtSkipass }, { entries: boughtSkipass.entries - 1 });
  }
}
