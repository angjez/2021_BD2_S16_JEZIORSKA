import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean } from 'class-validator';

export class UpdateBoughtSkipassDto {
  @ApiProperty()
  @IsBoolean()
  wasReturned: boolean;
}
