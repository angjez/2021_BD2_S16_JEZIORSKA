import { Module } from '@nestjs/common';
import { BoughtSkipassModule } from '../bought-skipass/bought-skipass.module';
import { EntryModule } from '../entry/entry.module';
import { LiftModule } from '../lift/lift.module';
import { ReportController } from './report.controller';
import { ReportService } from './report.service';

@Module({
  imports: [LiftModule, EntryModule, BoughtSkipassModule],
  controllers: [ReportController],
  providers: [ReportService],
  exports: [ReportService],
})
export class ReportModule {}
