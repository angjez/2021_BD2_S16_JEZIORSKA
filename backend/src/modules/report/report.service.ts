import { Inject, Injectable } from '@nestjs/common';
import * as PDFDocument from 'pdfkit';
import { BoughtSkipassService } from '../bought-skipass/bought-skipass.service';
import { EntryService } from '../entry/entry.service';
import { LiftService } from '../lift/lift.service';

@Injectable()
export class ReportService {
  constructor(
    @Inject(LiftService) private readonly liftService: LiftService,
    @Inject(EntryService) private readonly entryService: EntryService,
    @Inject(BoughtSkipassService) private readonly boughtSkipassService: BoughtSkipassService,
  ) {}

  loadPDFParams(doc: typeof PDFDocument) {
    doc.fontSize(14);
    doc.info['Author'] = 'Dec, Jeziorska, Migot, Zajęcki';
    doc.font('Helvetica');
  }

  async generateLiftReport(): Promise<Buffer> {
    const pdfBuffer: Buffer = await new Promise(async (resolve) => {
      const doc = new PDFDocument({
        size: 'A4',
        bufferPages: true,
      });

      let currentDate = new Date(Date.now());
      doc.fontSize(20).text(`Report generated at ${currentDate}`, 50, 50, {
        align: 'center',
      });
      this.loadPDFParams(doc);
      doc.text('Entries Today', 150, 150);
      doc.text('Entries in last\n7 days', 250, 150);
      doc.text('Entries in last\nmonth', 350, 150);
      doc.text('All entries', 450, 150);

      const getTodayEntries = async (id: number) => {
        const response = await this.entryService.getEntriesByNumberOfDays(id, 0);
        return response;
      };

      const getWeeklyEntries = async (id: number) => {
        const response = await this.entryService.getEntriesByNumberOfDays(id, 7);
        return response;
      };

      const getMonthlyEntries = async (id: number) => {
        const response = await this.entryService.getEntriesByNumberOfDays(id, 28);
        return response;
      };

      const getAllEntries = async (id: number) => {
        const response = await this.entryService.getEntriesByLiftId(id);
        return response;
      };

      const lifts = await this.liftService.getLifts();
      const pdfData = await Promise.all(
        lifts.map(async (lift) => ({
          id: lift.id,
          name: lift.name,
          isOperational: lift.isOperational,
          entriesToday: await getTodayEntries(lift.id),
          entriesWeekly: await getWeeklyEntries(lift.id),
          entriesMonthly: await getMonthlyEntries(lift.id),
          entriesAll: await getAllEntries(lift.id),
        })),
      );

      let yAxis = 200;

      pdfData.forEach(async (row) => {
        doc.text(row.name.toString(), 75, yAxis);
        doc.text(row.entriesToday.toLocaleString(), 150, yAxis);
        doc.text(`${row.entriesWeekly}`, 250, yAxis);
        doc.text(`${row.entriesMonthly}`, 350, yAxis);
        doc.text(`${row.entriesAll}`, 450, yAxis);
        doc
          .moveTo(0, yAxis - 15)
          .lineTo(700, yAxis - 15)
          .stroke();
        yAxis += 50;
      });

      doc.end();

      const buffer = [];
      doc.on('data', buffer.push.bind(buffer));
      doc.on('end', () => {
        const data = Buffer.concat(buffer);
        resolve(data);
      });
    });
    return pdfBuffer;
  }

  async generateSalesReport(): Promise<Buffer> {
    const pdfBuffer: Buffer = await new Promise(async (resolve) => {
      const doc = new PDFDocument({
        size: 'A4',
        bufferPages: true,
      });

      let currentDate = new Date(Date.now());
      doc.fontSize(20).text(`Report generated at ${currentDate}`, 50, 50, {
        align: 'center',
      });
      this.loadPDFParams(doc);
      doc.text('Skipass ID', 5, 150);
      doc.text('Entry Type', 80, 150);
      doc.text('Start', 262, 150);
      doc.text('Skier ID', 400, 150);
      doc.text('Returned', 460, 150);

      const boughtSkipasses = await this.boughtSkipassService.getAllBoughtSkipasses();
      console.log(boughtSkipasses);

      let yAxis = 200;

      boughtSkipasses.forEach(async (row) => {
        doc.text(row.skipassId.toString(), 25, yAxis);
        doc.text(row.entryType.toLocaleString(), 75, yAxis);
        doc.text(`${row.startsAt}`, 150, yAxis, {
          width: 250,
          align: 'center',
        });
        doc.text(`${row.skierId}`, 425, yAxis);
        doc.text(`${row.wasReturned}`, 475, yAxis);
        doc
          .moveTo(0, yAxis - 15)
          .lineTo(700, yAxis - 15)
          .stroke();
        yAxis += 50;
      });

      doc.end();

      const buffer = [];
      doc.on('data', buffer.push.bind(buffer));
      doc.on('end', () => {
        const data = Buffer.concat(buffer);
        resolve(data);
      });
    });
    return pdfBuffer;
  }
}
