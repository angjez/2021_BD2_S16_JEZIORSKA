import { Res, Get, Controller } from '@nestjs/common';
import { ReportService } from './report.service';
import { Response } from 'express';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
@ApiTags('report')
@Controller('report')
export class ReportController {
  constructor(private readonly reportService: ReportService) {}

  @Get('liftReport')
  @ApiOperation({ description: 'returns report from all entries' })
  async getLiftReport(@Res() res: Response): Promise<void> {
    const buffer = await this.reportService.generateLiftReport();

    res.set({
      'Content-Type': 'application/pdf',
      'Content-Disposition': 'attachment; filename=Lift_entries_report.pdf',
      'Content-Length': buffer.length,
    });

    res.end(buffer);
  }

  @Get('salesReport')
  @ApiOperation({ description: 'returns sales report from all sales' })
  async getSalesReport(@Res() res: Response): Promise<void> {
    const buffer = await this.reportService.generateSalesReport();

    res.set({
      'Content-Type': 'application/pdf',
      'Content-Disposition': 'attachment; filename=Sales_report.pdf',
      'Content-Length': buffer.length,
    });

    res.end(buffer);
  }
}
