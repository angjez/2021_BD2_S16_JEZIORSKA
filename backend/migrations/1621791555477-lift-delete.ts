import {MigrationInterface, QueryRunner} from "typeorm";

export class liftDelete1621791555477 implements MigrationInterface {
    name = 'liftDelete1621791555477'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lifts" ADD "is_deleted" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lifts" DROP COLUMN "is_deleted"`);
    }

}
