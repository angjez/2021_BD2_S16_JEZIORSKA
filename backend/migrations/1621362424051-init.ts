import {MigrationInterface, QueryRunner} from "typeorm";

export class init1621362424051 implements MigrationInterface {
    name = 'init1621362424051'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "skiers" ("id" SERIAL NOT NULL, "first_name" character varying NOT NULL, "last_name" character varying NOT NULL, "dob" TIMESTAMP NOT NULL, CONSTRAINT "PK_31b74d1cc16e091520ec2ab054f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "skipass_type_enum" AS ENUM('JUNIOR', 'STANDARD', 'SENIOR')`);
        await queryRunner.query(`CREATE TYPE "skipass_entry_type_enum" AS ENUM('TIME', 'ENTRIES')`);
        await queryRunner.query(`CREATE TABLE "skipass" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "type" "skipass_type_enum" NOT NULL, "entry_type" "skipass_entry_type_enum" NOT NULL, "duration_hours" integer, "entries" integer, "price" money NOT NULL, "starts_at" TIMESTAMP NOT NULL, "ends_at" TIMESTAMP NOT NULL, CONSTRAINT "PK_e254eeca9f9ca3ce7bf49da1ae9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "bought_skipasses_entry_type_enum" AS ENUM('TIME', 'ENTRIES')`);
        await queryRunner.query(`CREATE TABLE "bought_skipasses" ("id" SERIAL NOT NULL, "entry_type" "bought_skipasses_entry_type_enum" NOT NULL, "starts_at" TIMESTAMP, "ends_at" TIMESTAMP, "entries" integer, "was_returned" boolean NOT NULL DEFAULT false, "skier_id" integer NOT NULL, "skipass_id" integer NOT NULL, CONSTRAINT "PK_dbdc9dd7f3e931dff9c51de3859" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "lifts" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "is_operational" boolean NOT NULL, CONSTRAINT "PK_e5b7f1186f8fec4ac1f4b854000" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "entries" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL, "lift_id" integer NOT NULL, "bought_skipass_id" integer NOT NULL, "bought_skipasses_id" integer, CONSTRAINT "PK_23d4e7e9b58d9939f113832915b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "users" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "email" character varying NOT NULL, "password_hash" character varying NOT NULL, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "bought_skipasses" ADD CONSTRAINT "FK_37bb2ed8a096f4fdf8df1b563eb" FOREIGN KEY ("skier_id") REFERENCES "skiers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "bought_skipasses" ADD CONSTRAINT "FK_1f371bd2bb6068b6d995806ae5b" FOREIGN KEY ("skipass_id") REFERENCES "skipass"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "entries" ADD CONSTRAINT "FK_82ea06842d5bb3d7019d29971bc" FOREIGN KEY ("lift_id") REFERENCES "lifts"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "entries" ADD CONSTRAINT "FK_2fc658e4508dd1b9148453ac98e" FOREIGN KEY ("bought_skipasses_id") REFERENCES "bought_skipasses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entries" DROP CONSTRAINT "FK_2fc658e4508dd1b9148453ac98e"`);
        await queryRunner.query(`ALTER TABLE "entries" DROP CONSTRAINT "FK_82ea06842d5bb3d7019d29971bc"`);
        await queryRunner.query(`ALTER TABLE "bought_skipasses" DROP CONSTRAINT "FK_1f371bd2bb6068b6d995806ae5b"`);
        await queryRunner.query(`ALTER TABLE "bought_skipasses" DROP CONSTRAINT "FK_37bb2ed8a096f4fdf8df1b563eb"`);
        await queryRunner.query(`DROP TABLE "users"`);
        await queryRunner.query(`DROP TABLE "entries"`);
        await queryRunner.query(`DROP TABLE "lifts"`);
        await queryRunner.query(`DROP TABLE "bought_skipasses"`);
        await queryRunner.query(`DROP TYPE "bought_skipasses_entry_type_enum"`);
        await queryRunner.query(`DROP TABLE "skipass"`);
        await queryRunner.query(`DROP TYPE "skipass_entry_type_enum"`);
        await queryRunner.query(`DROP TYPE "skipass_type_enum"`);
        await queryRunner.query(`DROP TABLE "skiers"`);
    }

}
