import {MigrationInterface, QueryRunner} from "typeorm";

export class isDeletedSkipass1621363715084 implements MigrationInterface {
    name = 'isDeletedSkipass1621363715084'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "skipass" ADD "is_deleted" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "skipass" DROP COLUMN "is_deleted"`);
    }

}
